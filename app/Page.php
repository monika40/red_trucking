<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Page extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title',
        'sort_description',
        'description',
        'updated_at',
        'deleted_at',
    ];

       protected $table = 'pages';

    public static function update_Or_insert($postData,$id=0){
      
      if($id){
         $postData['updated_at']  =  date("Y-m-d H:i:s") ;
         $res  = DB::table('pages')->where('id',$id)->update($postData);  
         $result = $res ? $id : 0 ; 
      } else {
        $postData['created_at']  =  date("Y-m-d H:i:s") ;
         DB::table('pages')->insert($postData); 
        $result =  DB::getPdo()->lastInsertId();        
      }
      
       return $result;
    }

    public static function get_data_by_page_type($page_type,$status=1){

       $result = DB::table('pages')->
                    select('pages.*')->
                    where('pages.page_type',$page_type)->
                    where('pages.status',$status)->
                    where('pages.is_deleted',0)->
                    first();

       return $result;
    }

}
