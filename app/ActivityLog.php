<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class ActivityLog extends Model
{
    //

    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
      
        'parent_id',
        'user_id',
        'item_id',
        'item_type',
        'log_title',
        'log_text',
        'extra_data',
        'ip_address',
        'staus',
        'is_deleted',
        'updated_at',
        'deleted_at',
    ];

    protected $table = 'activity_log';

    public static function create_log($data){
      
        $data['extra_data']['ip_address'] = get_client_ip_server();
		$data['extra_data']['ip']         = $_SERVER['REMOTE_ADDR'];
        $data['extra_data']['log_timestamp'] = time();
        $data['extra_data']['log_created_at'] = date("Y-m-d h:i:s");
        $browser_info  = getBrowser();
        $data['extra_data']['browser_info']=$browser_info;
        $data['extra_data']['referal']= referrer();
        $data['extra_data']['os_name']= get_os_name();
		$data['extra_data']  = json_encode($data['extra_data']);
		$data['created_at']  =  date("Y-m-d H:i:s") ;
        $result = DB::table('activity_log')->insert($data);  
       return $result;
    }
}
