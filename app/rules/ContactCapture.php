<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ContactCapture implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($first_number,$second_number)
    {
        //
        $this->first_number  = $first_number ;
        $this->second_number = $second_number ;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        
         return ($this->first_number+$this->second_number) == $value ? true : false ;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid Capture';
    }
}
