<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Form;

class ContactusMail extends Mailable
{
    use Queueable, SerializesModels;

   
     public $data;
     public $config_data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($view_data,$config_data)
    {
        //
        $this->data = $view_data;
        $this->config_data = $config_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->subject(isset($this->config_data['subject'])? $this->config_data['subject']:'')->view(isset($this->config_data['view_file'])? $this->config_data['view_file']:'');
    }
}
