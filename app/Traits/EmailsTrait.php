<?php 

namespace App\Traits;

use App\Mail\ContactusMail;

use App\Form;

trait EmailsTrait {

	

	 public static  function contact_us_email($form_id) {
	 	
	 	$view_data   = [] ;
	 	$config_data = [] ;

	 	$view_data                 =  Form::get_data_by_form_id($form_id); 
	 	$config_data['subject']    = 'Conatct Us ' ;
	 	$config_data['view_file']  = 'emails.frontend.contactus' ;
	 	
         \Mail::to('ajam@plaxonic.com')->send(new ContactusMail($view_data,$config_data));
	 }

}