<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Form extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'transport_type',
        'form_type',
        'first_name',
        'last_name',
        'phone',
        'email',
        'message',
        'upload_file',
        'status',
        'is_deleted',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

       protected $table = 'forms';

    public static function update_Or_insert($postData,$id=0){
      
      if($id){
         $postData['updated_at']  =  date("Y-m-d H:i:s") ;
         $res  = DB::table('forms')->where('id',$id)->update($postData);  
         $result = $res ? $id : 0 ; 
      } else {
        $postData['created_at']  =  date("Y-m-d H:i:s") ;
         DB::table('forms')->insert($postData); 
        $result =  DB::getPdo()->lastInsertId();        
      }
      
       return $result;
    }

    public static function getAllFormtData($form_type,$page=10) {

       $result = DB::table('forms')->
                where('form_type',$form_type)->
                where('status',1)->
                where('is_deleted',0)->
                orderBy('id','DESC')->
                paginate($page);

       return $result;
    }

    public static function get_data_by_form_id($form_id){

       $result = DB::table('forms')->
                    select('forms.*')->
                    where('forms.id',$form_id)->
                    where('forms.is_deleted',0)->
                    first();

       return $result;
    }

   

}
