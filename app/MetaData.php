<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class MetaData extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'meta_title',
        'meta_type',
        'meta_data',
        'status',
    ];

       protected $table = 'meta_data';

    public static function update_Or_insert($postData,$id=0){
      
      if($id){
         $postData['updated_at']  =  date("Y-m-d H:i:s") ;
         $res  = DB::table('meta_data')->where('id',$id)->update($postData);  
         $result = $res ? $id : 0 ; 
      } else {
        $postData['created_at']  =  date("Y-m-d H:i:s") ;
         DB::table('meta_data')->insert($postData); 
        $result =  DB::getPdo()->lastInsertId();        
      }
      
       return $result;
    }

    public static function get_data_by_meta_type($meta_type,$status=1){

       $result = DB::table('meta_data')->
                    select('meta_data.*')->
                    where('meta_data.meta_type',$meta_type)->
                    where('meta_data.status',$status)->
                    where('meta_data.is_deleted',0)->
                    first();

       return $result;
    }

}
