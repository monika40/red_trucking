<?php
use App\MetaData;

if (!function_exists('create_slugify')) {
    function create_slugify($text) { 
       // replace non letter or digits by -
       $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
       // trim
       $text = trim($text, '-');
       // transliterate
       $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
       // lowercase
       $text = strtolower($text);
       // remove unwanted characters
       $text = preg_replace('~[^-\w]+~', '', $text);
       if (empty($text)) {
       return 'n-a';
       }
       return $text;
     }
}

if (!function_exists('pa')) {
    function pa($data) {
      echo '<pre>';
      print_r($data);
      echo '</pre>';        
    }
}

if (!function_exists('pad')) {
    function pad($data) {
    	echo '<pre>';
    	print_r($data);
    	echo '</pre>';    
    	die('die here...');    
    }
}

if ( ! function_exists('amount_format')) {
    function amount_format($amount){
        return number_format($amount,2,'.','');
    }
}

if ( ! function_exists('get_client_ip_server')) {
    function get_client_ip_server() {
        $ipaddress = '';
        
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
    
        return $ipaddress;
    }
}

if ( ! function_exists('get_client_location_by_id')) {
    function get_client_location_by_id($ip_address) {
        //$location_data = json_decode(file_get_contents("https://api.snoopi.io/$ip_address?apikey=".'AIzaSyDzmvaVuLuKqdm8QaFilgRd4sfP_c5hl04'),true);
        $location_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp??key=".'AIzaSyDzmvaVuLuKqdm8QaFilgRd4sfP_c5hl04'."&ip=".$ip_address));
        //$location_data = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?key=".'AIzaSyDzmvaVuLuKqdm8QaFilgRd4sfP_c5hl04'."&ip=".$ip_address."&format=json"));
        return $location_data ;
    }
}

if ( ! function_exists('objToArray')) {
    function objToArray($obj){
         if(is_object($obj)) $obj = (array) $obj;
           if(is_array($obj)) {
            $new = array();
              foreach($obj as $key => $val) {
              $new[$key] = objToArray($val);
               }
             }
           else $new = $obj;
         return $new;
         }
}


if ( ! function_exists('array_to_obj')) {
function array_to_obj($array, &$obj)
  {
    foreach ($array as $key => $value)
    {
      if (is_array($value))
      {
      $obj->$key = new stdClass();
      array_to_obj($value, $obj->$key);
      }
      else
      {
        $obj->$key = $value;
      }
    }
  return $obj;
  }
}  

if ( ! function_exists('arrayToObject')) {
    function arrayToObject($array)
    {
    $object= new stdClass();
    return array_to_obj($array,$object);
    }
}

if ( ! function_exists('humanTiming')) {
    function humanTiming ($date_time) {
         $time = strtotime($date_time) ;

        $time = time() - $time; // to get the time since that moment
        $time = ($time<1)? 1 : $time;
        $tokens = array (
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
        }

    }
}

if ( ! function_exists('hoursRange')) {
    function hoursRange( $lower = 0, $upper = 86400, $step = 3600, $format = '' ) {
        $times = array();
        if ( empty( $format ) ) {
            $format = 'g:i a';
        }
        foreach ( range( $lower, $upper, $step ) as $increment ) {
            $increment = gmdate( 'H:i', $increment );
            list( $hour, $minutes ) = explode( ':', $increment );
            $date = new DateTime( $hour . ':' . $minutes );
            $times[(string) $increment] = $date->format( $format );
        }
        return $times;
    }
}

 if ( ! function_exists('get_location_details_by_lat_long')) {
        function get_location_details_by_lat_long($lat,$long) {
                    $data=array();
                    $request =  'https://maps.googleapis.com/maps/api/geocode/json?key='.'AIzaSyDzmvaVuLuKqdm8QaFilgRd4sfP_c5hl04'.'&latlng='.trim($lat).','.trim($long).'&sensor=false';
                    $file_contents = file_get_contents($request);
                    $json_decode = json_decode($file_contents);
                    if(isset($json_decode) && !empty($json_decode))
                    {
                        $add_array  = $json_decode->results;
                        $add_array = $add_array[0];
                        $add_array = $add_array->address_components;
                    
                        foreach ($add_array as $key) 
                        {
                              if($key->types[0]=='political')
                              {
                                $area=$key->long_name;
                              }
                              if($key->types[0] == 'administrative_area_level_2')
                              {
                                    $city = $key->long_name;
                              }
                              if($key->types[0] == 'administrative_area_level_1')
                              {
                                    $state = $key->long_name;
                              }
                              if($key->types[0] == 'country')
                              {
                                    $country = $key->long_name;
                              }
                         }
                         $data = array(
                                'area'=> isset($area) ? $area :'',
                                                    'city'=> isset($city) ? $city :'',
                                                    'state'=> isset($state) ? $state :'',
                                                    'country'=> isset($country) ? $country :'',
                                        ) ;
                    }
                    
                     return $data;
          }
    }

    if ( ! function_exists('get_lat_long_by_address'))
    {
        function get_lat_long_by_address($address)
        {
            $data = array() ;
            $formattedAddr     = str_replace(' ','+',$address);
            //$api_url = "https://maps.google.com/maps/api/geocode/json?address=$formattedAddr&sensor=false&region=India&key=".GOOGLE_MAP_LOCATION_KEY;
            $api_url = "https://maps.google.com/maps/api/geocode/json?address=$formattedAddr&sensor=false&key=".GOOGLE_MAP_LOCATION_KEY;
            $geocodeFromAddr   = file_get_contents($api_url);
            $output            = json_decode($geocodeFromAddr,true);
            $data['latitude']  = isset($output['results'][0]['geometry']['location']['lat']) ? $output['results'][0]['geometry']['location']['lat'] : '' ;
            $data['longitude'] = isset($output['results'][0]['geometry']['location']['lng']) ? $output['results'][0]['geometry']['location']['lng']:'';
            return $data;       

        }
    }

    if ( ! function_exists('getBrowser')) {
    function getBrowser() {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";
      
        // First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
          $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
          $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
          $platform = 'windows';
        }
      
        // Next get the name of the useragent yes seperately and for good reason
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) {
          $bname = 'Internet Explorer';
          $ub = "MSIE";
        } elseif(preg_match('/Firefox/i',$u_agent)) {
          $bname = 'Mozilla Firefox';
          $ub = "Firefox";
        } elseif(preg_match('/Chrome/i',$u_agent)) {
          $bname = 'Google Chrome';
          $ub = "Chrome";
        } elseif(preg_match('/Safari/i',$u_agent)) {
          $bname = 'Apple Safari';
          $ub = "Safari";
        } elseif(preg_match('/Opera/i',$u_agent)) {
          $bname = 'Opera';
          $ub = "Opera";
        } elseif(preg_match('/Netscape/i',$u_agent)) {
          $bname = 'Netscape';
          $ub = "Netscape";
        }
      
        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
          // we have no matching number just continue
        }
      
        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
          //we will have two since we are not using 'other' argument yet
          //see if version is before or after the name
          if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
          } else {
            $version= $matches['version'][1];
          }
        } else {
          $version= $matches['version'][0];
        }
      
        // check if we have a number
        if ($version==null || $version=="") {$version="?";}
      
      return array(
        'userAgent' => $u_agent,
        'browser_name' => $bname,
        'browser_version'   => $version,
        'os_platform'  => $platform,
        'pattern'    => $pattern
        );
      }
    }

    if ( ! function_exists('get_browser_name')) {
    function get_browser_name() {
            if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)
              return 'Internet explorer';
            elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== false)
                return 'Internet explorer';
            elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') !== false)
              return 'Mozilla Firefox';
            elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== false)
              return 'Google Chrome';
            elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Opera Mini') !== false)
              return "Opera Mini";
            elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Opera') !== false)
              return "Opera";
            elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') !== false)
              return "Safari";
            else
              return 'Other';
      }
    }  

    if ( ! function_exists('referrer')) { 
       function referrer () {
        return isset($_SERVER['HTTP_REFERER']) ?$_SERVER['HTTP_REFERER'] :''  ;
      }
    } 

    if ( ! function_exists('get_os_name')) {
      function get_os_name() { 

        $user_agent = $_SERVER['HTTP_USER_AGENT'];
      
        $os_platform =   "Bilinmeyen İşletim Sistemi";
        $os_array =   array(
          '/windows nt 10/i'      =>  'Windows 10',
          '/windows nt 6.3/i'     =>  'Windows 8.1',
          '/windows nt 6.2/i'     =>  'Windows 8',
          '/windows nt 6.1/i'     =>  'Windows 7',
          '/windows nt 6.0/i'     =>  'Windows Vista',
          '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
          '/windows nt 5.1/i'     =>  'Windows XP',
          '/windows xp/i'         =>  'Windows XP',
          '/windows nt 5.0/i'     =>  'Windows 2000',
          '/windows me/i'         =>  'Windows ME',
          '/win98/i'              =>  'Windows 98',
          '/win95/i'              =>  'Windows 95',
          '/win16/i'              =>  'Windows 3.11',
          '/macintosh|mac os x/i' =>  'Mac OS X',
          '/mac_powerpc/i'        =>  'Mac OS 9',
          '/linux/i'              =>  'Linux',
          '/ubuntu/i'             =>  'Ubuntu',
          '/iphone/i'             =>  'iPhone',
          '/ipod/i'               =>  'iPod',
          '/ipad/i'               =>  'iPad',
          '/android/i'            =>  'Android',
          '/blackberry/i'         =>  'BlackBerry',
          '/webos/i'              =>  'Mobile'
        );
      
        foreach ( $os_array as $regex => $value ) { 
          if ( preg_match($regex, $user_agent ) ) {
            $os_platform = $value;
          }
        }   
        return $os_platform;
      } 
    }
     if ( ! function_exists('get_browser_language')) { 
        function get_browser_language () {
        return substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);  
      } 
    }


  if (!function_exists('get_social_media_links')) {
    function get_social_media_links(){    
     
      $social_media_data = MetaData::get_data_by_meta_type(config('constants.meta_type.social_media_link'));

       $social_media_link = isset($social_media_data->meta_data) && !empty($social_media_data->meta_data) ? json_decode($social_media_data->meta_data,true) : array() ;
       return $social_media_link ;
   
     
    }
  }

if (!function_exists('get_contact_us_data')) {
    function get_contact_us_data(){    
     
     $contact_us_data = MetaData::get_data_by_meta_type(config('constants.meta_type.contact_us_page_details'));

     $contact_us_meta = isset($contact_us_data->meta_data) && !empty($contact_us_data->meta_data) ? json_decode($contact_us_data->meta_data) : array() ;

       return $contact_us_meta ;
   
     
    }
  }

