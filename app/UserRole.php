<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'user_id',
        'role_id',
    ];

       protected $table = 'users_roles';
}
