<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Blog extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        
        'title',
        'blog_url',
        'phone',
        'description',
        'image',
        'status',
        'is_deleted',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

       protected $table = 'blogs';

       


    public static function update_Or_insert($postData,$id=0){
      
      if(isset($postData['title'])){
            $postData['blog_url']  =  create_slugify($postData['title']) ;
        }

      if($id){
         $postData['updated_at']  =  date("Y-m-d H:i:s") ;
         $res  = DB::table('blogs')->where('id',$id)->update($postData);  
         $result = $res ? $id : 0 ; 
      } else {
         $postData['created_at']  =  date("Y-m-d H:i:s") ;
         DB::table('blogs')->insert($postData); 
        $result =  DB::getPdo()->lastInsertId();        
      }
      
       return $result;
    }

    public static function getAllBlogData($page=10) {

       $result = DB::table('blogs')->
                where('is_deleted',0)->
                orderBy('id','DESC')->
                paginate($page);

       return $result;
    }

    public static function is_exists($id){

       $result = DB::table('blogs')->
                    select('blogs.id')->
                    where('blogs.id',$id)->
                    where('blogs.is_deleted',0)->
                    first();

       return !empty($result) ? 1  : 0 ;
    }

    public static function check_unique($name,$id=0) {
        $returnData = ''  ;
        if(isset($name) && !empty($name)) {
            if(!empty($id) && ($id !== 0)){
             
                 $returnData = DB::table('blogs')->
                    select('blogs.*')->
                    where('blogs.id','!=',$id)->
                    where('blogs.blog_url',create_slugify(trim($name)))->
                    where('blogs.is_deleted',0)->
                    first();
            } else {
                $returnData = DB::table('blogs')->
                    select('blogs.id')->
                    where('blogs.blog_url',create_slugify(trim($name)))->
                    where('blogs.is_deleted',0)->
                    first();

            }
            
        } 
        return $returnData ;
    }


    public static function get_data_by_id($id){

       $result = DB::table('blogs')->
                    select('blogs.*')->
                    where('blogs.id',$id)->
                    where('blogs.is_deleted',0)->
                    first();

       return $result;
    }
    public static function get_data_by_blog_url($blog_url){

       $result = DB::table('blogs')->
                    select('blogs.*')->
                    where('blogs.blog_url',$blog_url)->
                    where('blogs.is_deleted',0)->
                    first();

       return $result;
    }

   

}
