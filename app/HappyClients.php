<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class HappyClients extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        
        'name',
        'image',
        'url',
        'status',
        'is_deleted',
    ];

       protected $table = 'happy_clients';

       


    public static function update_Or_insert($postData,$id=0){
      
      if($id){
         $postData['updated_at']  =  date("Y-m-d H:i:s") ;
         $res  = DB::table('happy_clients')->where('id',$id)->update($postData);  
         $result = $res ? $id : 0 ; 
      } else {
         $postData['created_at']  =  date("Y-m-d H:i:s") ;
         DB::table('happy_clients')->insert($postData); 
        $result =  DB::getPdo()->lastInsertId();        
      }
      
       return $result;
    }

    public static function getRandomData($limit=6) {

       $result = DB::table('happy_clients')->
                where('is_deleted',0)->
                 inRandomOrder()->
                limit($limit)->
                get();

       return $result;
    }

     public static function getAllClientData($page=10) {

       $result = DB::table('happy_clients')->
                where('is_deleted',0)->
                 orderBy('id','DESC')->
                paginate($page);

       return $result;
    }

    public static function is_exists($id){

       $result = DB::table('happy_clients')->
                    select('happy_clients.id')->
                    where('happy_clients.id',$id)->
                    where('happy_clients.is_deleted',0)->
                    first();

       return !empty($result) ? 1  : 0 ;
    }

   


    public static function get_data_by_id($id){

       $result = DB::table('happy_clients')->
                    select('happy_clients.*')->
                    where('happy_clients.id',$id)->
                    where('happy_clients.is_deleted',0)->
                    first();

       return $result;
    }

   

}
