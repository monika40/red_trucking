<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Blog;
class BlogController   extends Controller
{
    //

    public function index(Request $request)
    {   
     $page_title     = 'Blog- '.config('constants.website_name') ;
     $description     = 'Read our blogs to keep yourself updated on the transportation management system and logistics.' ;
     $blog_data       = Blog::getAllBlogData(9);

     $og_meta = [
                  'title'=>$page_title,
                  'site_name'=>config('constants.website_name').' - Blogs',
                  'url'=>'blogs',
                  'description'=>$description,
                  'image'=>isset($blog_data[0]->image) && file_exists($blog_data[0]->image) ? $blog_data[0]->image:'' ,
                  ];
      

     return view('frontend.blog.index',compact('page_title','description','blog_data','og_meta'));

    }
    public function detail($blog_url)
    {   
      $page_title     = 'Blog Details' ;
      $blog_data = Blog::get_data_by_blog_url($blog_url);
      $id = isset($blog_data->id) ? $blog_data->id :  0 ; 
     if(!Blog::is_exists($id)){
       	 return redirect()->back();
       }
       $recent_blog_data = Blog::whereNotIn('id', [$id])->where('is_deleted',0)->where('status',1)->orderBy('id', 'asc')->take(2)->get();
       // pa($id);
       // pa($recent_blog_data);
       // foreach ($recent_blog_data->items as $key => $value) {
       // 	 pa($value);
       // }
       // die ;

     return view('frontend.blog.details',compact('page_title','blog_data','recent_blog_data'));

    }
    
}
