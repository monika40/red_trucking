<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Page;
use App\PageGallery;

class PartnerController extends Controller
{
    //

     public function index(Request $request)
    {   

    	 $data = Page::get_data_by_page_type(config('constants.partner_page'));

    	 $image_data = PageGallery::get_data_by_page_id(isset($data->id) ? $data->id : 0 ,config('constants.page_gallery_type.image'));
    	
        return view('admin.partner.partner',compact('data','image_data'));
    }

     public function update(Request $request)
    {
      
      $request->validate(
                [
                    'title' => 'required',
                    'sort_description' => 'required|max:255',
                    'description' => 'required',
                    //'images' => 'required|image|mimes:jpeg,png,jpg,gif',
                ]
        );

 

            $post   = $request->all();
            $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;
            $gallery_id  =  isset($post['gallery_id']) && !empty($post['gallery_id']) ? $post['gallery_id'] : 0;
            $postData =  array(
                    'title'=>isset($post['title'])? $post['title'] : '',
                    'sort_description'=>isset($post['sort_description'])? $post['sort_description'] : '',
                    'description'=>isset($post['description'])? $post['description'] : '',
                    'page_type'=>config('constants.partner_page'),
                    'status'=>1,
                    'is_deleted'=>0,
            );

        $res = Page::update_Or_insert($postData,$id);

          $images = $request->file('images'); 
          if(isset($images) && !empty($images) && $res ) {
          	  $fileName = time().'.'.$images->getClientOriginalExtension();  
              $images->move('uploads/about', $fileName);
              $uploded_file_name = 'uploads/about/'.$fileName;

              $gal_id  = PageGallery::update_Or_insert(['page_id'=>$res,'type'=>config('constants.page_gallery_type.image'),'name'=>$uploded_file_name],$gallery_id);
               if(isset($post['gallery_image_name']) && !empty($post['gallery_image_name'])) {
               		\File::delete($post['gallery_image_name']);
               }

          }
        

     
   
            $flush_data =  array('key'=>'','msg'=>'') ;
            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Partner updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Partner save successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }          

 
        return $res ? redirect('admin/partner')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.partner.index')->with($flush_data['key'], $flush_data['msg']);
    

    }


}
