<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\MetaData;
use App\HappyClients;
use App\Testimonials;



class HomePageController   extends Controller
{
    //
    public function index(Request $request)
    {   
      $about_us_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.about_us_home_content'));

      $trust_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.trust_home_content'));
      $security_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.security_home_content'));
      $sustainability_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.sustainability_home_content'));
      $service_areas_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.service_areas_home_content'));
      $red_service_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.red_service_home_content'));
      $partner_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.partner_home_content'));
      $resource_meta_data = MetaData::get_data_by_meta_type(config('constants.meta_type.resource_home_content'));

      $all_clients = HappyClients::getAllClientData();
      $testimonial_page =  isset($request['testimonial_page']) ? $request['testimonial_page'] : 10 ;
      $all_testimonials = Testimonials::getAllTestimonialsData($testimonial_page);
      $all_testimonials->setPageName('testimonial_page') ;
      

    	  return view('admin.home_page.index',compact('about_us_meta_data','trust_meta_data','security_meta_data','sustainability_meta_data','service_areas_meta_data','red_service_meta_data','partner_meta_data','resource_meta_data','all_clients','all_testimonials'));
    }



     public function update(Request $request)
    {

      $post   = $request->all();
      ////////// About us Validation /////////////
      $validation =  array(
                    'about_us_content' => 'required',
                    'about_us_title' => 'required|max:355',
                     ) ;

      $about_us_id  = isset($post['about_us_id']) && !empty($post['about_us_id']) ? $post['about_us_id'] : 0 ;

      if(!$about_us_id) {
         $validation['about_us_upload_video'] = 'required|mimes:mp4,mov,ogg,qt | max:102400' ;  // 100 MB 
         $validation['about_us_video_image'] = 'required|image|mimes:jpeg,png,jpg,gif'; 
      }
       $about_us_upload_video = $request->file('about_us_upload_video'); 
     
      if(isset($about_us_upload_video) && !empty($about_us_upload_video) ) {
        $validation['about_us_upload_video'] = 'required|mimes:mp4,mov,ogg | max:102400' ; // 100 MB 
      }
      ////////// About us Validation /////////////

      ////////// TRUST Validation /////////////
      $validation['trust_title']   = 'required'; 
      $validation['trust_content'] = 'required|max:355'; 

      $trust_id  = isset($post['trust_id']) && !empty($post['trust_id']) ? $post['trust_id'] : 0 ;
      ////////// TRUST Validation /////////////

      ////////// SECURITY Validation /////////////
      $validation['security_title']   = 'required'; 
      $validation['security_content'] = 'required|max:355'; 

      $security_id  = isset($post['security_id']) && !empty($post['security_id']) ? $post['security_id'] : 0 ;
      ////////// SECURITY Validation /////////////

      ////////// SUSTAINABILITY Validation /////////////
      $validation['sustainability_title']   = 'required'; 
      $validation['sustainability_content'] = 'required|max:355'; 

      $sustainability_id  = isset($post['sustainability_id']) && !empty($post['sustainability_id']) ? $post['sustainability_id'] : 0 ;
      ////////// SUSTAINABILITY Validation /////////////

      ////////// SERVICE AREAS Validation /////////////
      $validation['service_areas_title']   = 'required'; 
      $validation['service_areas_content'] = 'required|max:355'; 

      $service_areas_id  = isset($post['service_areas_id']) && !empty($post['service_areas_id']) ? $post['service_areas_id'] : 0 ;
      ////////// SERVICE AREAS Validation /////////////

      ////////// RED SERVICES Validation /////////////
      $validation['red_service_title']   = 'required'; 
      $validation['red_service_content'] = 'required|max:355'; 

      $red_service_id  = isset($post['red_service_id']) && !empty($post['red_service_id']) ? $post['red_service_id'] : 0 ;
      ////////// RED SERVICES Validation /////////////

      ////////// Partner Validation /////////////
      // $validation['partner_title']   = 'required'; 
      // $validation['partner_content'] = 'required|max:355'; 

      // $partner_id  = isset($post['partner_id']) && !empty($post['partner_id']) ? $post['partner_id'] : 0 ;
      ////////// Partner Validation /////////////


      ////////// Resource Validation /////////////
      $validation['resource_title']   = 'required'; 
      $validation['resource_content'] = 'required|max:355'; 

      $resource_id  = isset($post['resource_id']) && !empty($post['resource_id']) ? $post['resource_id'] : 0 ;
      ////////// Resource Validation /////////////


      $request->validate($validation);

      ////////// About Us Save Content /////////////

      $about_us_meta_data = [
                             'content'=>isset($post['about_us_content'])? $post['about_us_content'] : '',
                             'video_url'=>isset($post['about_us_upload_video'])? $post['about_us_upload_video'] : '',
                            ];

      $about_us_upload_video = $request->file('about_us_upload_video'); 
      
      if(isset($about_us_upload_video) && !empty($about_us_upload_video) ) {
              $fileName = time().'.'.$about_us_upload_video->getClientOriginalExtension();  
              $about_us_upload_video->move('uploads/meta', $fileName);
              $uploded_file_name = 'uploads/meta/'.$fileName;
              $about_us_meta_data['video_url'] =  $uploded_file_name ;

      } else {
        if(isset($post['old_about_us_upload_video']) && !empty($post['old_about_us_upload_video'])){
          $about_us_meta_data['video_url'] =  $post['old_about_us_upload_video'] ;
        }

      }


      $about_us_video_image = $request->file('about_us_video_image'); 
      
      if(isset($about_us_video_image) && !empty($about_us_video_image) ) {
              $fileName = time().'.'.$about_us_video_image->getClientOriginalExtension();  
              $about_us_video_image->move('uploads/meta', $fileName);
              $uploded_file_name = 'uploads/meta/'.$fileName;
              $about_us_meta_data['video_poster_image'] =  $uploded_file_name ;

      } else {
        if(isset($post['old_about_us_video_image']) && !empty($post['old_about_us_video_image'])){
          $about_us_meta_data['video_poster_image'] =  $post['old_about_us_video_image'] ;
        }

      }


      

      $aboutData =  array(
                    'meta_type'=>config('constants.meta_type.about_us_home_content'),
                    'meta_title'=>isset($post['about_us_title'])? $post['about_us_title'] : '',
                    'meta_data'=>json_encode($about_us_meta_data),
            );
      $res = MetaData::update_Or_insert($aboutData,$about_us_id);


      ////////// About Us Save Content /////////////

      ////////// Trust Save Content /////////////

      $trust_meta_data = [
                             'content'=>isset($post['trust_content'])? $post['trust_content'] : '',
                            ];
      $trustData =  array(
                    'meta_type'=>config('constants.meta_type.trust_home_content'),
                    'meta_title'=>isset($post['trust_title'])? $post['trust_title'] : '',
                    'meta_data'=>json_encode($trust_meta_data),
            );                     
      $res = MetaData::update_Or_insert($trustData,$trust_id);

      ////////// Trust Save Content /////////////

      ////////// Security Save Content /////////////

      $security_meta_data = [
                             'content'=>isset($post['security_content'])? $post['security_content'] : '',
                            ];
      $securityData =  array(
                    'meta_type'=>config('constants.meta_type.security_home_content'),
                    'meta_title'=>isset($post['security_title'])? $post['security_title'] : '',
                    'meta_data'=>json_encode($security_meta_data),
            );                     
      $res = MetaData::update_Or_insert($securityData,$security_id);

      ////////// Security Save Content /////////////

      ////////// Sustainability Save Content /////////////

      $sustainability_meta_data = [
                             'content'=>isset($post['sustainability_content'])? $post['sustainability_content'] : '',
                            ];
      $sustainabilityData =  array(
                    'meta_type'=>config('constants.meta_type.sustainability_home_content'),
                    'meta_title'=>isset($post['sustainability_title'])? $post['sustainability_title'] : '',
                    'meta_data'=>json_encode($sustainability_meta_data),
            );                     
      $res = MetaData::update_Or_insert($sustainabilityData,$sustainability_id);

      ////////// Sustainability Save Content /////////////

      ////////// SERVICE AREAS Save Content /////////////

      $service_areas_meta_data = [
                             'content'=>isset($post['service_areas_content'])? $post['service_areas_content'] : '',
                            ];
      $sservice_areasData =  array(
                    'meta_type'=>config('constants.meta_type.service_areas_home_content'),
                    'meta_title'=>isset($post['service_areas_title'])? $post['service_areas_title'] : '',
                    'meta_data'=>json_encode($service_areas_meta_data),
            );                     
      $res = MetaData::update_Or_insert($sservice_areasData,$service_areas_id);

      ////////// SERVICE AREAS Save Content /////////////

      ////////// RED SERVICES Save Content /////////////

      $red_service_meta_data = [
                             'content'=>isset($post['red_service_content'])? $post['red_service_content'] : '',
                            ];
      $red_serviceData =  array(
                    'meta_type'=>config('constants.meta_type.red_service_home_content'),
                    'meta_title'=>isset($post['red_service_title'])? $post['red_service_title'] : '',
                    'meta_data'=>json_encode($red_service_meta_data),
            );                     
      $res = MetaData::update_Or_insert($red_serviceData,$red_service_id);

      ////////// RED SERVICES Save Content /////////////

      ////////// Partner Save Content /////////////

      // $partner_meta_data = [
      //                        'content'=>isset($post['partner_content'])? $post['partner_content'] : '',
      //                       ];
      // $partnerData =  array(
      //               'meta_type'=>config('constants.meta_type.partner_home_content'),
      //               'meta_title'=>isset($post['partner_title'])? $post['partner_title'] : '',
      //               'meta_data'=>json_encode($partner_meta_data),
      //       );                     
      // $res = MetaData::update_Or_insert($partnerData,$partner_id);

      ////////// Partner Save Content /////////////

      ////////// Resource Save Content /////////////

      $resource_meta_data = [
                             'content'=>isset($post['resource_content'])? $post['resource_content'] : '',
                            ];
      $resourceData =  array(
                    'meta_type'=>config('constants.meta_type.resource_home_content'),
                    'meta_title'=>isset($post['resource_title'])? $post['resource_title'] : '',
                    'meta_data'=>json_encode($resource_meta_data),
            );                     
      $res = MetaData::update_Or_insert($resourceData,$resource_id);

      ////////// Resource Save Content /////////////
     
   
            $flush_data =  array('key'=>'','msg'=>'') ;
                
            $flush_data['key'] = 'success' ;
            $flush_data['msg'] = 'Home page save successfully' ;

 
        return $res ? redirect('admin/homepage')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.about.index')->with($flush_data['key'], $flush_data['msg']);
    

    }

    public function addclient(Request $request)
    {   
     

        return view('admin.home_page.add_client');
    }

      public function updateclient(Request $request)
    {

       $flush_data =  array('key'=>'','msg'=>'') ;
       $post   = $request->all();

       $validation =  array(
                    'name' => 'required',
                    'url' => 'required|url',
                     ) ;
       
       $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;
       if(!$id || !empty($request->file('images'))){
           $validation['images'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=100,min_height=30' ;
         } 

          $customMessages = [
                            'images.dimensions' => 'Select :attribute image must be at least min 100x30 pixels.',
                        ]; 


        $request->validate($validation,$customMessages);

        $postData =  array(
                    'name'=>isset($post['name'])? $post['name'] : '',
                    'url'=>isset($post['url'])? $post['url'] : '',
                 );

      

      $images = $request->file('images'); 
          if(isset($images) && !empty($images) ) {
              $fileName = time().'.'.$images->getClientOriginalExtension();  
              $images->move('uploads/happyclient', $fileName);
              $uploded_file_name = 'uploads/happyclient/'.$fileName;
              $postData['image'] = $uploded_file_name ; 
              
          }

         $res = HappyClients::update_Or_insert($postData,$id);


         
          
          if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Client updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Client added successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            } 

        return $res ? redirect('admin/homepage')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.homepage.addclient')->with($flush_data['key'], $flush_data['msg']);
    


    }


     public function editclient($id)
    {   
        if(! HappyClients::is_exists($id) ){
          return redirect('admin/homepage')->with('error', 'Happy Client does not exist');
        }
        $data = HappyClients::get_data_by_id($id);
        return view('admin.home_page.add_client',compact('data'));
    }

    







    
}
