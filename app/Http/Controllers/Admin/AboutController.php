<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Page;
use App\PageGallery;
use App\MetaData;
use App\CompanyMembers;

class AboutController   extends Controller
{
    //

    public function index(Request $request)
    {   

    	 $data = Page::get_data_by_page_type(config('constants.about_us_page.about_us'));

    	 $left_image_data = PageGallery::get_data_by_page_id(isset($data->id) ? $data->id : 0 ,config('constants.page_gallery_type.image'));

        $right_image_data = PageGallery::get_right_images_by_page_id(isset($data->id) ? $data->id : 0 ,config('constants.page_gallery_type.image'));

        $about_us_owner_data = MetaData::get_data_by_meta_type(config('constants.meta_type.about_us_owner_data'));

        $red_family_data = MetaData::get_data_by_meta_type(config('constants.meta_type.about_us_red_family_content'));
        $safety_data = MetaData::get_data_by_meta_type(config('constants.meta_type.safety_about_us_content'));
        $sustainability_data = MetaData::get_data_by_meta_type(config('constants.meta_type.sustainability_about_us_content'));
       
        $all_members = CompanyMembers::getAllMemberData();
        return view('admin.about.aboutus',compact('data','left_image_data','right_image_data','about_us_owner_data','all_members','red_family_data','safety_data','sustainability_data'));
    }



    public function addmember(Request $request)
    {   

       $data = array() ;
       

        return view('admin.about.add_member',compact('data'));
    }
    public function editmember($id)
    {   
        if(! CompanyMembers::is_exists($id) ){
          return redirect('admin/about')->with('error', 'Family Member does not exist');
        }
        $data = CompanyMembers::get_data_by_id($id);
       

        return view('admin.about.add_member',compact('data'));
    }

    public function updatemember(Request $request)
    {   

      $flush_data =  array('key'=>'','msg'=>'') ;
      $post   = $request->all();
      $validation = [
                    'name' => 'required',
                    'designation' => 'required',
                ] ;

        $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;
        !$id || !empty($request->file('images')) ? $validation['images'] = 'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=360,max_height=540' : '' ;

        $customMessages = [
                            'images.dimensions' => 'Select :attribute image must be at least min 360x540 pixels.',
                        ];
          
       $request->validate($validation,$customMessages);
    
      $postData =  array(
                    'name'=>isset($post['name'])? $post['name'] : '',
                    'designation'=>isset($post['designation'])? $post['designation'] : '',
                 );

      

      $images = $request->file('images'); 
          if(isset($images) && !empty($images) ) {
              $fileName = time().'.'.$images->getClientOriginalExtension();  
              $images->move('uploads/members', $fileName);
              $uploded_file_name = 'uploads/members/'.$fileName;
              $postData['image'] = $uploded_file_name ; 
              
          }

         $res = CompanyMembers::update_Or_insert($postData,$id);

            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Member updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Member added successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            } 

        return $res ? redirect('admin/about')->with($flush_data['key'], $flush_data['msg'])  : redirect()->back()->with($flush_data['key'], $flush_data['msg']);

    }

     public function updateabout(Request $request)
    {
      $validation  = [
                    'owner_message' => 'required',
                    'designation' => 'required',
                    'our_mission' => 'required',
                    'red_family_content' => 'required',
                    'description' => 'required',
                    'safety_content' => 'required',
                    'sustainability_content' => 'required',
                    //'images' => 'required|image|mimes:jpeg,png,jpg,gif',
                ] ;
    
 

            $post   = $request->all();
            $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;
            $gallery_id  =  isset($post['gallery_id']) && !empty($post['gallery_id']) ? $post['gallery_id'] : 0;
            $right_gallery_id  =  isset($post['right_gallery_id']) && !empty($post['right_gallery_id']) ? $post['right_gallery_id'] : 0;
            $meta_id  =  isset($post['meta_id']) && !empty($post['meta_id']) ? $post['meta_id'] : 0;
            $red_family_content_id  =  isset($post['red_family_content_id']) && !empty($post['red_family_content_id']) ? $post['red_family_content_id'] : 0;
            $safety_id  =  isset($post['safety_id']) && !empty($post['safety_id']) ? $post['safety_id'] : 0;
            $sustainability_id  =  isset($post['sustainability_id']) && !empty($post['sustainability_id']) ? $post['sustainability_id'] : 0;
            $postData =  array(
                    'title'=>isset($post['title'])? $post['title'] : '',
                    'sort_description'=>isset($post['our_mission'])? $post['our_mission'] : '',
                    'description'=>isset($post['description'])? $post['description'] : '',
                    'page_type'=>config('constants.about_us_page.about_us'),
                    'status'=>1,
                    'is_deleted'=>0,
            );

         if(!$gallery_id || !empty($request->file('images'))){
           $validation['images'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=840,min_height=452' ;
         } 
          if(!$right_gallery_id || !empty($request->file('right_images'))){
           $validation['right_images'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=840,min_height=452' ;
         } 
         if(!$meta_id || !empty($request->file('signature')) ){
           $validation['signature'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=100,min_height=30,max_width=300,max_height=92' ;
         }  

         if(!$safety_id || !empty($request->file('safety_images'))){
           $validation['safety_images'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=855,min_height=444' ;
         }
         if(!$sustainability_id || !empty($request->file('sustainability_images'))){
           $validation['sustainability_images'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=855,min_height=444' ;
         }

          $customMessages = [
                            'signature.dimensions' => 'Select :attribute image must be at least min 100x30 to 300x92 pixels.',
                            'images.dimensions' => 'Select :attribute  must be at least min 840x452 pixels.',
                            'right_images.dimensions' => 'Select :attribute  must be at least min 840x452 pixels.',
                            'safety_images.dimensions' => 'Select :attribute  must be at least min 855x444 pixels.',
                            'sustainability_images.dimensions' => 'Select :attribute  must be at least min 855x444 pixels.',
                        ]; 
      
        $request->validate($validation,$customMessages);

        $res = Page::update_Or_insert($postData,$id);

          $images = $request->file('images'); 
          if(isset($images) && !empty($images) && $res ) {
          	  $fileName = time().'.t'.$images->getClientOriginalExtension();  
              $images->move('uploads/about', $fileName);
              $uploded_file_name = 'uploads/about/'.$fileName;

              $gal_id  = PageGallery::update_Or_insert(['page_id'=>$res,'type'=>config('constants.page_gallery_type.image'),'name'=>$uploded_file_name],$gallery_id);
               if(isset($post['gallery_image_name']) && !empty($post['gallery_image_name'])) {
               		\File::delete($post['gallery_image_name']);
               }

          }

          $right_images = $request->file('right_images'); 
          if(isset($right_images) && !empty($right_images) && $res ) {
              $fileName = time().'.'.$right_images->getClientOriginalExtension();  
              $right_images->move('uploads/about', $fileName);
              $uploded_file_name = 'uploads/about/'.$fileName;

              $gal_id  = PageGallery::update_Or_insert(['page_id'=>$res,'type'=>config('constants.page_gallery_type.image'),'name'=>$uploded_file_name],$right_gallery_id);

               if(isset($post['right_gallery_image_name']) && !empty($post['right_gallery_image_name'])) {
                  \File::delete($post['right_gallery_image_name']);
               }

          }


           ////////// About Us Owner Save Content /////////////
           $about_us_owner_meta_data = [
                             'owner_message'=>isset($post['owner_message'])? $post['owner_message'] : '',
                             'designation'=>isset($post['designation'])? $post['designation'] : '',
                            ];
            $signature = $request->file('signature'); 
                  
            if(isset($signature) && !empty($signature) ) {
                    $fileName = time().'.'.$signature->getClientOriginalExtension();  
                    $signature->move('uploads/owner', $fileName);
                    $uploded_file_name = 'uploads/owner/'.$fileName;
                    $about_us_owner_meta_data['signature'] =  $uploded_file_name ;

            } else {
              if(isset($post['old_signature_image']) && !empty($post['old_signature_image'])){
                $about_us_owner_meta_data['signature'] =  $post['old_signature_image'] ;
              }

            }
             $metaData =  array(
                    'meta_type'=>config('constants.meta_type.about_us_owner_data'),
                    'meta_title'=> 'About Us Owner ',
                    'meta_data'=>json_encode($about_us_owner_meta_data),
            );
            MetaData::update_Or_insert($metaData,$meta_id);

           ////////// About Us Owner Save Content /////////////

            ////////// About Us Owner Save Content /////////////
           $red_family_meta_data = [
                             'content'=>isset($post['red_family_content'])? $post['red_family_content'] : '',
                            ];
          
             $metaData =  array(
                    'meta_type'=>config('constants.meta_type.about_us_red_family_content'),
                    'meta_title'=> 'THE RED FAMILY',
                    'meta_data'=>json_encode($red_family_meta_data),
            );
            MetaData::update_Or_insert($metaData,$red_family_content_id);

           ////////// About Us Owner Save Content /////////////


          ////////// Safety Save Content /////////////
           $safety_meta_data = [
                             'content'=>isset($post['safety_content'])? $post['safety_content'] : '',
                            ];

            $safety_images = $request->file('safety_images'); 
                  
            if(isset($safety_images) && !empty($safety_images) ) {
                    $fileName = time().'.'.$safety_images->getClientOriginalExtension();  
                    $safety_images->move('uploads/safety', $fileName);
                    $uploded_file_name = 'uploads/safety/'.$fileName;
                    $safety_meta_data['safety_images'] =  $uploded_file_name ;

            } else {
              if(isset($post['old_safety_images']) && !empty($post['old_safety_images'])){
                $safety_meta_data['safety_images'] =  $post['old_safety_images'] ;
              }

            }
                            
          
             $metaData =  array(
                    'meta_type'=>config('constants.meta_type.safety_about_us_content'),
                    'meta_title'=> 'SAFETY',
                    'meta_data'=>json_encode($safety_meta_data),
            );
            MetaData::update_Or_insert($metaData,$safety_id);

          ////////// Saftey Save Content /////////////

          ////////// Sustainability Save Content /////////////
           $sustainability_meta_data = [
                             'content'=>isset($post['sustainability_content'])? $post['sustainability_content'] : '',
                            ];

            $sustainability_images = $request->file('sustainability_images'); 
                  
            if(isset($sustainability_images) && !empty($sustainability_images) ) {
                    $fileName = time().'.'.$sustainability_images->getClientOriginalExtension();  
                    $sustainability_images->move('uploads/sustainability', $fileName);
                    $uploded_file_name = 'uploads/sustainability/'.$fileName;
                    $sustainability_meta_data['sustainability_images'] =  $uploded_file_name ;

            } else {
              if(isset($post['old_sustainability_images']) && !empty($post['old_sustainability_images'])){
                $sustainability_meta_data['sustainability_images'] =  $post['old_sustainability_images'] ;
              }

            }
                            
          
             $metaData =  array(
                    'meta_type'=>config('constants.meta_type.sustainability_about_us_content'),
                    'meta_title'=> 'SUSTAINABILITY',
                    'meta_data'=>json_encode($sustainability_meta_data),
            );
            MetaData::update_Or_insert($metaData,$sustainability_id);

          ////////// Sustainability Save Content /////////////
        

     
   
            $flush_data =  array('key'=>'','msg'=>'') ;
            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'About updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'About us  save successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }          

 
        return $res ? redirect('admin/about')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.about.index')->with($flush_data['key'], $flush_data['msg']);
    

    }


    public function safety(Request $request)
    {   

    	 
       $safety_data = MetaData::get_data_by_meta_type(config('constants.meta_type.safety_page'));
    	
        return view('admin.about.safety',compact('safety_data'));
    }
    public function updatesafety(Request $request)
    {
      
      $validation  =
                      [
                          'title' => 'required|max:355',
                          'left_content' => 'required',
                          'middle_content' => 'required',
                          'right_content' => 'required',
                      ];

      $post   = $request->all();
      $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;

      if(!$id || !empty($request->file('left_image'))){
        $validation['left_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=460,min_height=290' ;
       }
       if(!$id || !empty($request->file('right_image'))){
        $validation['right_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=460,min_height=290' ;
       }

        $customMessages = [
                            'left_image.dimensions' => 'Select :attribute image must be at least min 340x290 pixels.',
                            'right_image.dimensions' => 'Select :attribute image must be at least min 340x290 pixels.',
                        ];
          
       $request->validate($validation,$customMessages); 

             
            
            $postData =  array(
                    'title'=>isset($post['title'])? $post['title'] : '',
                    'left_content'=>isset($post['left_content'])? $post['left_content'] : '',
                    'middle_content'=>isset($post['middle_content'])? $post['middle_content'] : '',
                    'right_content'=>isset($post['right_content'])? $post['right_content'] : '',
            );

          $left_image = $request->file('left_image'); 

          if(isset($left_image) && !empty($left_image) ) {

              $fileName = time().'left.'.$left_image->getClientOriginalExtension();  
              $left_image->move('uploads/safety', $fileName);
              $uploded_file_name = 'uploads/safety/'.$fileName;
              $postData['left_image'] =  $uploded_file_name ;

               if(isset($post['old_left_image']) && !empty($post['old_left_image'])) {
                    \File::delete($post['old_left_image']);
               }

          } else {
              if(isset($post['old_left_image']) && !empty($post['old_left_image'])){
                $postData['left_image'] =  $post['old_left_image'] ;
              }

            }

          $right_image = $request->file('right_image'); 
          
          if(isset($right_image) && !empty($right_image) ) {

              $fileName = time().'right.'.$right_image->getClientOriginalExtension();  
              $right_image->move('uploads/safety', $fileName);
              $uploded_file_name = 'uploads/safety/'.$fileName;
              $postData['right_image'] =  $uploded_file_name ;

               if(isset($post['old_right_image']) && !empty($post['old_right_image'])) {
                    \File::delete($post['old_right_image']);
               }

          } else {
              if(isset($post['old_right_image']) && !empty($post['old_right_image'])){
                $postData['right_image'] =  $post['old_right_image'] ;
              }

            }
            
          $metaData =  array(
                    'meta_type'=>config('constants.meta_type.safety_page'),
                    'meta_title'=> 'Saftey Page ',
                    'meta_data'=>json_encode($postData),
            );
            $res = MetaData::update_Or_insert($metaData,$id);

            $flush_data =  array('key'=>'','msg'=>'') ;
            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Safety updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Safety saved successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }

          


 
        return $res ? redirect('admin/about/safety')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.about.safety')->with($flush_data['key'], $flush_data['msg']);
    

    }

    public function sustainability(Request $request)
    {   

      $sustainability_data  = MetaData::get_data_by_meta_type(config('constants.meta_type.sustainability_page'));
      return view('admin.about.sustainability',compact('sustainability_data'));
    }
    public function updatesustainability(Request $request)
    {       
            $post   = $request->all();

            $validation  =
                      [
                          'first_content' => 'required',
                          'second_content' => 'required',
                          'third_content' => 'required',
                          'bottom_content' => 'required',
                      ];
      
     
            $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;

            

            if(!$id || !empty($request->file('first_image'))){
               $validation['first_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=470,min_height=286' ;
            }
            if(!$id || !empty($request->file('second_image'))){
               $validation['second_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=544,min_height=226' ;
            }
            if(!$id || !empty($request->file('third_image'))){
               $validation['third_image'] =  'required|image|mimes:jpeg,png,jpg,gif|dimensions:min_width=470,min_height=286' ;
            }

            $customMessages = [
                            'first_image.dimensions' => 'Select :attribute image must be at least min 470x286 pixels.',
                            'second_image.dimensions' => 'Select :attribute image must be at least min 544x226 pixels.',
                            'third_image.dimensions' => 'Select :attribute image must be at least min 470x286 pixels.',
                        ];


            $request->validate($validation);


            $postData =  array(
                    'first_content'=>isset($post['first_content'])? $post['first_content'] : '',
                    'second_content'=>isset($post['second_content'])? $post['second_content'] : '',
                    'third_content'=>isset($post['third_content'])? $post['third_content'] : '',
                    'bottom_content'=>isset($post['bottom_content'])? $post['bottom_content'] : '',
                   );

          $first_image = $request->file('first_image'); 
          
          if(isset($first_image) && !empty($first_image) ) {

              $fileName = time().'-.'.$first_image->getClientOriginalExtension();  
              $first_image->move('uploads/sustainability', $fileName);
              $uploded_file_name = 'uploads/sustainability/'.$fileName;
              $postData['first_image'] =  $uploded_file_name ;

               if(isset($post['old_first_image']) && !empty($post['old_first_image'])) {
                    \File::delete($post['old_first_image']);
               }

          } else {
              if(isset($post['old_first_image']) && !empty($post['old_first_image'])){
                $postData['first_image'] =  $post['old_first_image'] ;
              }

          }
          $second_image = $request->file('second_image'); 
          if(isset($second_image) && !empty($second_image) ) {

              $fileName = time().'--.'.$second_image->getClientOriginalExtension();  
              $second_image->move('uploads/sustainability', $fileName);
              $uploded_file_name = 'uploads/sustainability/'.$fileName;
              $postData['second_image'] =  $uploded_file_name ;

               if(isset($post['old_second_image']) && !empty($post['old_second_image'])) {
                    \File::delete($post['old_first_image']);
               }

          } else {
              if(isset($post['old_second_image']) && !empty($post['old_second_image'])){
                $postData['second_image'] =  $post['old_second_image'] ;
              }

          }

          $third_image = $request->file('third_image'); 
          if(isset($third_image) && !empty($third_image) ) {

              $fileName = time().'---.'.$third_image->getClientOriginalExtension();  
              $third_image->move('uploads/sustainability', $fileName);
              $uploded_file_name = 'uploads/sustainability/'.$fileName;
              $postData['third_image'] =  $uploded_file_name ;

               if(isset($post['old_third_image']) && !empty($post['old_third_image'])) {
                    \File::delete($post['old_third_image']);
               }

          } else {
              if(isset($post['old_third_image']) && !empty($post['old_third_image'])){
                $postData['third_image'] =  $post['old_third_image'] ;
              }

          }
          
          $metaData =  array(
                    'meta_type'=>config('constants.meta_type.sustainability_page'),
                    'meta_title'=> 'About Sustainability',
                    'meta_data'=>json_encode($postData),
            );

            $res = MetaData::update_Or_insert($metaData,$id);


            $flush_data =  array('key'=>'','msg'=>'') ;
            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Sustainability updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Sustainability saved successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }          

 
        return $res ? redirect('admin/about/sustainability')->with($flush_data['key'], $flush_data['msg'])  : redirect()->route('admin.about.sustainability')->with($flush_data['key'], $flush_data['msg']);
    

    }

    
}
