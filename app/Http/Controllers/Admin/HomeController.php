<?php

namespace App\Http\Controllers\Admin;

use App\Form;

class HomeController
{
    public function index()
    { 

    	 $contact_form_data = Form::getAllFormtData(config('constants.form_type.contact'));
    	 $total_contact = $contact_form_data->count();
    	 $career_form_data = Form::getAllFormtData(config('constants.form_type.career'));
    	 $total_career = $career_form_data->count();
        return view('admin.home.index',compact('total_contact','total_career'));
    }
}
