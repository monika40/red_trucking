<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\Banner;
use App\ActivityLog;

class BannerController extends Controller
{
    //

     public function index(Request $request)
    {   
      //pad(env('APP_URL'));
    	$image_data = Banner::get_banner_data();
    	$sr = 1 ;
        return view('admin.banner.index',compact('image_data','sr'));
    }

     public function update(Request $request)
    {

    
        $post   = $request->all();
        $flush_data =  array('key'=>'','msg'=>'') ;
            
        $page_gallery = isset($post['page_gallery']) && sizeof($post['page_gallery']) ? $post['page_gallery'] : array() ; 
        
        foreach ($page_gallery['title'] as $key => $value) {

          $banner_id = isset($page_gallery['banner_id'][$key]) && !empty($page_gallery['banner_id'][$key]) ? $page_gallery['banner_id'][$key] : 0 ;

          $images = $request->file("page_gallery.image.$key"); 

          $gallery_data = [
            'title'=>isset($page_gallery['title'][$key]) ? $page_gallery['title'][$key] : 0 ,
            'details'=>isset($page_gallery['details'][$key]) ? $page_gallery['details'][$key] : '' ,
            'logo_title'=>isset($page_gallery['logo_title'][$key]) ? $page_gallery['logo_title'][$key] : '' ,
            ];

          if(isset($images) && !empty($images)) {
              $fileName = time().'.'.$images->getClientOriginalExtension();  
              $images->move('uploads/banner', $fileName);
              $uploded_file_name = 'uploads/banner/'.$fileName;
              $gallery_data['image'] = $uploded_file_name ;
          }

          $background_image = $request->file("page_gallery.background_image.$key"); 
          if(isset($background_image) && !empty($background_image)) {
              $fileName = time().'.'.$background_image->getClientOriginalExtension();  
              $background_image->move('uploads/banner/background_image', $fileName);
              $uploded_file_name = 'uploads/banner/background_image/'.$fileName;
              $gallery_data['background_image'] = $uploded_file_name ;
          }
          

          $second_image = $request->file("page_gallery.second_image.$key"); 
          if(isset($second_image) && !empty($second_image)) {
              $fileName = time().'.'.$second_image->getClientOriginalExtension();  
              $second_image->move('uploads/banner/image', $fileName);
              $uploded_file_name = 'uploads/banner/image/'.$fileName;
              $gallery_data['second_image'] = $uploded_file_name ;
          }

          $third_image = $request->file("page_gallery.third_image.$key"); 
          if(isset($third_image) && !empty($third_image)) {
              $fileName = time().'.'.$third_image->getClientOriginalExtension();  
              $third_image->move('uploads/banner/image', $fileName);
              $uploded_file_name = 'uploads/banner/image/'.$fileName;
              $gallery_data['third_image'] = $uploded_file_name ;
          }

          $logo_image = $request->file("page_gallery.logo_image.$key"); 
          if(isset($logo_image) && !empty($logo_image)) {
              $fileName = time().'.'.$logo_image->getClientOriginalExtension();  
              $logo_image->move('uploads/banner/logo', $fileName);
              $uploded_file_name = 'uploads/banner/logo/'.$fileName;
              $gallery_data['logo_image'] = $uploded_file_name ;
          }


          $gal_id  = Banner::update_Or_insert($gallery_data,$banner_id);


          //////// ****************** Log Activity ****************///////////////////
          $extra_data = ['server_data'=>$_SERVER,'session_data'=>Session::all()];
          $log_data = [
                      'user_id'              => \Auth::user()->id,
                      'item_id'              => $banner_id,
                      'item_type'            => config('constants.activity_item_type.banner'), 
                      'log_title'            => $banner_id ?' Banner Updated' : 'Banner Added ' ,
                      'log_text'             => $banner_id ?' Banner Updated #'.$banner_id : 'Banner Added #'.$gal_id ,
                      'extra_data'            => $extra_data,
                      'ip_address'            => get_client_ip_server(),
                      ]; 
          ActivityLog::create_log($log_data);
          //////////////// ********************************///////////////////
         
          
        }
            $flush_data['key'] = 'success' ;
            $flush_data['msg'] = 'Banner save successfully' ;
         
 
      return  redirect()->back()->with($flush_data['key'], $flush_data['msg']);
    

    }

    public function delete($id)
    {  
        $postData = [
                    'is_deleted'=>1,
                    'deleted_at'=>date("Y-m-d H:i:s") ,
                    ];

         $res = Banner::update_Or_insert($postData,$id);

         if($res){  
                    //////// ****************** Log Activity ****************///////////////////
                    $extra_data = ['server_data'=>$_SERVER,'session_data'=>Session::all()];
                    $log_data = [
                                'user_id'              => \Auth::user()->id,
                                'item_id'              => $id,
                                'item_type'            => config('constants.activity_item_type.banner'), 
                                'log_title'            =>  'Banner Deleted',
                                'log_text'             => ' Banner Deleted #'.$id ,
                                'extra_data'            => $extra_data,
                                'ip_address'            => get_client_ip_server(),
                                ]; 
                    ActivityLog::create_log($log_data);
                    //////////////// ********************************///////////////////

                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Banner deleted successfully' ;

            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            }

            return  redirect()->back()->with($flush_data['key'], $flush_data['msg']);
    }


}
