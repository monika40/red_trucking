<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Validator;
use Session;
use App\MetaData;

class ContactController   extends Controller
{
    //

    public function index(Request $request)
    {   

        
        $contact_us_data = MetaData::get_data_by_meta_type(config('constants.meta_type.contact_us_page_details'));

        return view('admin.contactus.index',compact('contact_us_data'));
    }



   

    public function update(Request $request)
    {   

      $flush_data =  array('key'=>'','msg'=>'') ;
      $post   = $request->all();

      $validation = [
                    'red_headquarter_address' => 'required',
                    'red_headquarter_address1' => 'required',
                    'red_headquarter_phone' => 'required',
                    'red_headquarter_email' => 'required',
                    'red_headquarter_email_google_map_link' => 'required',
                    'warehouse_address' => 'required',
                    'warehouse_address1' => 'required',
                    'warehouse_phone' => 'required',
                    'warehouse_email' => 'required',
                    'warehouse_google_map_link' => 'required',
                    'sales_address' => 'required',
                    'sales_address1' => 'required',
                    'sales_phone' => 'required',
                    'sales_email' => 'required',
                    'sales_google_map_link' => 'required',

                    'recruiting_address' => 'required',
                    'recruiting_address1' => 'required',
                    'recruiting_phone' => 'required',
                    'recruiting_email' => 'required',
                    'recruiting_google_map_link' => 'required',

                    'google_embed_map' => 'required',
                ] ;

        $id     =  isset($post['id']) && !empty($post['id']) ? $post['id'] : 0;

        $customMessages = [
                            'red_headquarter_address.required' => 'The Building/Appartment/Floor field is required.',
                            'red_headquarter_address1.required' => 'The Address field is required.',
                            'red_headquarter_phone.required' => 'The Phone No  field is required.',
                            'red_headquarter_email.required' => 'The Email  field is required.',
                            'red_headquarter_email_google_map_link.required' => 'The Google Map Link field is required.',
                            'warehouse_address.required' => 'The Building/Appartment/Floor field is required.',
                            'warehouse_address1.required' => 'The Address field is required.',
                            'warehouse_phone.required' => 'The Phone No  field is required.',
                            'warehouse_email.required' => 'The Email  field is required.',
                            'warehouse_google_map_link.required' => 'The Google Map Link field is required.',
                            'sales_address.required' => 'The Building/Appartment/Floor field is required.',
                            'sales_address1.required' => 'The Address  field is required.',
                            'sales_phone.required' => 'The Phone No field is required.',
                            'sales_email.required' => 'The Email field is required.',
                            'sales_google_map_link.required' => 'The Google Map Link field is required.',

                            'recruiting_address.required' => 'The Building/Appartment/Floor field is required.',
                            'recruiting_address1.required' => 'The Address  field is required.',
                            'recruiting_phone.required' => 'The Phone No field is required.',
                            'recruiting_email.required' => 'The Email field is required.',
                            'recruiting_google_map_link.required' => 'The Google Map Link field is required.',

                            'google_embed_map.required' => 'The Embed Map is required.',
                        ];
          
       $request->validate($validation,$customMessages);
    
      $postData =  array(
                    'red_headquarter_address'=>isset($post['red_headquarter_address'])? $post['red_headquarter_address'] : '',
                    'red_headquarter_address1'=>isset($post['red_headquarter_address1'])? $post['red_headquarter_address1'] : '',
                    'red_headquarter_phone'=>isset($post['red_headquarter_phone'])? $post['red_headquarter_phone'] : '',
                    'red_headquarter_email'=>isset($post['red_headquarter_email'])? $post['red_headquarter_email'] : '',
                    'red_headquarter_email_google_map_link'=>isset($post['red_headquarter_email_google_map_link'])? $post['red_headquarter_email_google_map_link'] : '',
                    'warehouse_address'=>isset($post['warehouse_address'])? $post['warehouse_address'] : '',
                    'warehouse_address1'=>isset($post['warehouse_address1'])? $post['warehouse_address1'] : '',
                    'warehouse_phone'=>isset($post['warehouse_phone'])? $post['warehouse_phone'] : '',
                    'warehouse_email'=>isset($post['warehouse_email'])? $post['warehouse_email'] : '',
                    'warehouse_google_map_link'=>isset($post['warehouse_google_map_link'])? $post['warehouse_google_map_link'] : '',
                    'sales_address'=>isset($post['sales_address'])? $post['sales_address'] : '',
                    'sales_address1'=>isset($post['sales_address1'])? $post['sales_address1'] : '',
                    'sales_phone'=>isset($post['sales_phone'])? $post['sales_phone'] : '',
                    'sales_email'=>isset($post['sales_email'])? $post['sales_email'] : '',
                    'sales_google_map_link'=>isset($post['sales_google_map_link'])? $post['sales_google_map_link'] : '',

                    'recruiting_address'=>isset($post['recruiting_address'])? $post['recruiting_address'] : '',
                    'recruiting_address1'=>isset($post['recruiting_address1'])? $post['recruiting_address1'] : '',
                    'recruiting_phone'=>isset($post['recruiting_phone'])? $post['recruiting_phone'] : '',
                    'recruiting_email'=>isset($post['recruiting_email'])? $post['recruiting_email'] : '',
                    'recruiting_google_map_link'=>isset($post['recruiting_google_map_link'])? $post['recruiting_google_map_link'] : '',

                    'google_embed_map'=>isset($post['google_embed_map'])? $post['google_embed_map'] : '',
                    
                 );

     

      
                $metaData =  array(
                                    'meta_type'=>config('constants.meta_type.contact_us_page_details'),
                                    'meta_title'=> 'Contact Us',
                                    'meta_data'=>json_encode($postData),
                            );
               
            $res = MetaData::update_Or_insert($metaData,$id);

            if($res){
                if($id){
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Contact Us updated successfully' ;
                } else {
                    $flush_data['key'] = 'success' ;
                    $flush_data['msg'] = 'Contact Us added successfully' ;
                }
            } else {
                $flush_data['erro'] = 'error' ;
                $flush_data['msg'] = 'some thing went to wrong !' ;
            } 

        return $res ? redirect('admin/contactus')->with($flush_data['key'], $flush_data['msg'])  : redirect()->back()->with($flush_data['key'], $flush_data['msg']);

    }


    
}
