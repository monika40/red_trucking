<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class CompanyMembers extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        
        'name',
        'image',
        'designation',
        'status',
        'is_deleted',
    ];

       protected $table = 'company_members';

       


    public static function update_Or_insert($postData,$id=0){
      
      if($id){
         $postData['updated_at']  =  date("Y-m-d H:i:s") ;
         $res  = DB::table('company_members')->where('id',$id)->update($postData);  
         $result = $res ? $id : 0 ; 
      } else {
         $postData['created_at']  =  date("Y-m-d H:i:s") ;
         DB::table('company_members')->insert($postData); 
        $result =  DB::getPdo()->lastInsertId();        
      }
      
       return $result;
    }

    public static function getAllMemberData($page=10) {

       $result = DB::table('company_members')->
                where('is_deleted',0)->
                paginate($page);

       return $result;
    }

    public static function is_exists($id){

       $result = DB::table('company_members')->
                    select('company_members.id')->
                    where('company_members.id',$id)->
                    where('company_members.is_deleted',0)->
                    first();

       return !empty($result) ? 1  : 0 ;
    }

   


    public static function get_data_by_id($id){

       $result = DB::table('company_members')->
                    select('company_members.*')->
                    where('company_members.id',$id)->
                    where('company_members.is_deleted',0)->
                    first();

       return $result;
    }

   

}
