@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        <h4><b>Contact Us</b></h4>
    </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/contactus/update') }}" method="POST" enctype="multipart/form-data">
            @csrf
           
            @php
            $contact_us_meta = isset($contact_us_data->meta_data) && !empty($contact_us_data->meta_data) ? json_decode($contact_us_data->meta_data) : array() ;
            @endphp 

             <label for="red_headquarter">RED HQ</label>

            <div class="form-group {{ $errors->has('red_headquarter_address') ? 'has-error' : '' }}">
                <label for="designation">Building/Appartment/Floor*</label>
                <input type="text" id="red_headquarter_address" name="red_headquarter_address" class="form-control" value="{{ old('red_headquarter_address', isset($contact_us_meta->red_headquarter_address) ? $contact_us_meta->red_headquarter_address : '') }}">
                @if($errors->has('red_headquarter_address'))
                    <p class="help-block">
                        {{ $errors->first('red_headquarter_address') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('red_headquarter_address1') ? 'has-error' : '' }}">
                <label for="red_headquarter_address1">Address*</label>
                <input type="text" id="red_headquarter_address1" name="red_headquarter_address1" class="form-control" value="{{ old('red_headquarter_address1', isset($contact_us_meta->red_headquarter_address1) ? $contact_us_meta->red_headquarter_address1 : '') }}">
                @if($errors->has('red_headquarter_address1'))
                    <p class="help-block">
                        {{ $errors->first('red_headquarter_address1') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('red_headquarter_phone') ? 'has-error' : '' }}">
                <label for="red_headquarter_phone">Phone No*</label>
                <input type="text" id="red_headquarter_phone" name="red_headquarter_phone" class="form-control" value="{{ old('red_headquarter_phone', isset($contact_us_meta->red_headquarter_phone) ? $contact_us_meta->red_headquarter_phone : '') }}">
                @if($errors->has('red_headquarter_phone'))
                    <p class="help-block">
                        {{ $errors->first('red_headquarter_phone') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('red_headquarter_email') ? 'has-error' : '' }}">
                <label for="red_headquarter_email">Email*</label>
                <input type="text" id="red_headquarter_email" name="red_headquarter_email" class="form-control" value="{{ old('red_headquarter_email', isset($contact_us_meta->red_headquarter_email) ? $contact_us_meta->red_headquarter_email : '') }}">
                @if($errors->has('red_headquarter_email'))
                    <p class="help-block">
                        {{ $errors->first('red_headquarter_email') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('red_headquarter_email_google_map_link') ? 'has-error' : '' }}">
                <label for="red_headquarter_email_google_map_link">Google Map Link*</label>
                <input type="text" id="red_headquarter_email_google_map_link" name="red_headquarter_email_google_map_link" class="form-control" value="{{ old('red_headquarter_email_google_map_link', isset($contact_us_meta->red_headquarter_email_google_map_link) ? $contact_us_meta->red_headquarter_email_google_map_link : '') }}">
                @if($errors->has('red_headquarter_email_google_map_link'))
                    <p class="help-block">
                        {{ $errors->first('red_headquarter_email_google_map_link') }}
                    </p>
                @endif
                
            </div>

            <hr/>

             <label for="warehouse_address">WAREHOUSE</label>

            <div class="form-group {{ $errors->has('warehouse_address') ? 'has-error' : '' }}">
                <label for="warehouse_address">Building/Appartment/Floor*</label>
                <input type="text" id="warehouse_address" name="warehouse_address" class="form-control" value="{{ old('warehouse_address', isset($contact_us_meta->warehouse_address) ? $contact_us_meta->warehouse_address : '') }}">
                @if($errors->has('warehouse_address'))
                    <p class="help-block">
                        {{ $errors->first('warehouse_address') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('warehouse_address1') ? 'has-error' : '' }}">
                <label for="warehouse_address1">Address*</label>
                <input type="text" id="warehouse_address1" name="warehouse_address1" class="form-control" value="{{ old('warehouse_address1', isset($contact_us_meta->warehouse_address1) ? $contact_us_meta->warehouse_address1 : '') }}">
                @if($errors->has('warehouse_address1'))
                    <p class="help-block">
                        {{ $errors->first('warehouse_address1') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('warehouse_phone') ? 'has-error' : '' }}">
                <label for="warehouse_phone">Phone No*</label>
                <input type="text" id="warehouse_phone" name="warehouse_phone" class="form-control" value="{{ old('warehouse_phone', isset($contact_us_meta->warehouse_phone) ? $contact_us_meta->warehouse_phone : '') }}">
                @if($errors->has('warehouse_phone'))
                    <p class="help-block">
                        {{ $errors->first('warehouse_phone') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('warehouse_email') ? 'has-error' : '' }}">
                <label for="warehouse_email">Email*</label>
                <input type="text" id="warehouse_email" name="warehouse_email" class="form-control" value="{{ old('warehouse_email', isset($contact_us_meta->warehouse_email) ? $contact_us_meta->warehouse_email : '') }}">
                @if($errors->has('warehouse_email'))
                    <p class="help-block">
                        {{ $errors->first('warehouse_email') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('warehouse_google_map_link') ? 'has-error' : '' }}">
                <label for="warehouse_google_map_link">Google Map Link*</label>
                <input type="text" id="warehouse_google_map_link" name="warehouse_google_map_link" class="form-control" value="{{ old('warehouse_google_map_link', isset($contact_us_meta->warehouse_google_map_link) ? $contact_us_meta->warehouse_google_map_link : '') }}">
                @if($errors->has('warehouse_google_map_link'))
                    <p class="help-block">
                        {{ $errors->first('warehouse_google_map_link') }}
                    </p>
                @endif
                
            </div>

            <hr/>

             <label for="sales_phone">SALES</label>
            
            <div class="form-group {{ $errors->has('sales_address') ? 'has-error' : '' }}">
                <label for="warehouse_address1">Building/Appartment/Floor*</label>
                <input type="text" id="sales_address" name="sales_address" class="form-control" value="{{ old('sales_address', isset($contact_us_meta->sales_address) ? $contact_us_meta->sales_address : '') }}">
                @if($errors->has('sales_address'))
                    <p class="help-block">
                        {{ $errors->first('sales_address') }}
                    </p>
                @endif
                
            </div>
            
            <div class="form-group {{ $errors->has('sales_address1') ? 'has-error' : '' }}">
                <label for="warehouse_address1">Address*</label>
                <input type="text" id="sales_address1" name="sales_address1" class="form-control" value="{{ old('sales_address1', isset($contact_us_meta->sales_address1) ? $contact_us_meta->sales_address1 : '') }}">
                @if($errors->has('sales_address1'))
                    <p class="help-block">
                        {{ $errors->first('sales_address1') }}
                    </p>
                @endif
                
            </div>
           
            <div class="form-group {{ $errors->has('sales_phone') ? 'has-error' : '' }}">
                <label for="sales_phone">Phone No*</label>
                <input type="text" id="sales_phone" name="sales_phone" class="form-control" value="{{ old('sales_phone', isset($contact_us_meta->sales_phone) ? $contact_us_meta->sales_phone : '') }}">
                @if($errors->has('sales_phone'))
                    <p class="help-block">
                        {{ $errors->first('sales_phone') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('sales_email') ? 'has-error' : '' }}">
                <label for="sales_email">Email*</label>
                <input type="text" id="sales_email" name="sales_email" class="form-control" value="{{ old('sales_email', isset($contact_us_meta->sales_email) ? $contact_us_meta->sales_email : '') }}">
                @if($errors->has('sales_email'))
                    <p class="help-block">
                        {{ $errors->first('sales_email') }}
                    </p>
                @endif
                
            </div>
            
            <div class="form-group {{ $errors->has('sales_google_map_link') ? 'has-error' : '' }}">
                <label for="sales_google_map_link">Google Map Link*</label>
                <input type="text" id="sales_google_map_link" name="sales_google_map_link" class="form-control" value="{{ old('sales_google_map_link', isset($contact_us_meta->sales_google_map_link) ? $contact_us_meta->sales_google_map_link : '') }}">
                @if($errors->has('sales_google_map_link'))
                    <p class="help-block">
                        {{ $errors->first('sales_google_map_link') }}
                    </p>
                @endif
                
            </div>

            


            <hr/>

             <label for="sales_phone">Recruiting</label>
            
            <div class="form-group {{ $errors->has('recruiting_address') ? 'has-error' : '' }}">
                <label for="warehouse_address1">Building/Appartment/Floor*</label>
                <input type="text" id="recruiting_address" name="recruiting_address" class="form-control" value="{{ old('recruiting_address', isset($contact_us_meta->recruiting_address) ? $contact_us_meta->recruiting_address : '') }}">
                @if($errors->has('recruiting_address'))
                    <p class="help-block">
                        {{ $errors->first('recruiting_address') }}
                    </p>
                @endif
                
            </div>
            
            <div class="form-group {{ $errors->has('recruiting_address1') ? 'has-error' : '' }}">
                <label for="warehouse_address1">Address*</label>
                <input type="text" id="recruiting_address1" name="recruiting_address1" class="form-control" value="{{ old('recruiting_address1', isset($contact_us_meta->recruiting_address1) ? $contact_us_meta->recruiting_address1 : '') }}">
                @if($errors->has('recruiting_address1'))
                    <p class="help-block">
                        {{ $errors->first('recruiting_address1') }}
                    </p>
                @endif
                
            </div>
           
            <div class="form-group {{ $errors->has('recruiting_phone') ? 'has-error' : '' }}">
                <label for="recruiting_phone">Phone No*</label>
                <input type="text" id="recruiting_phone" name="recruiting_phone" class="form-control" value="{{ old('recruiting_phone', isset($contact_us_meta->recruiting_phone) ? $contact_us_meta->recruiting_phone : '') }}">
                @if($errors->has('recruiting_phone'))
                    <p class="help-block">
                        {{ $errors->first('recruiting_phone') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('recruiting_email') ? 'has-error' : '' }}">
                <label for="recruiting_email">Email*</label>
                <input type="text" id="recruiting_email" name="recruiting_email" class="form-control" value="{{ old('recruiting_email', isset($contact_us_meta->recruiting_email) ? $contact_us_meta->recruiting_email : '') }}">
                @if($errors->has('recruiting_email'))
                    <p class="help-block">
                        {{ $errors->first('recruiting_email') }}
                    </p>
                @endif
                
            </div>
            
            <div class="form-group {{ $errors->has('recruiting_google_map_link') ? 'has-error' : '' }}">
                <label for="recruiting_google_map_link">Google Map Link*</label>
                <input type="text" id="recruiting_google_map_link" name="recruiting_google_map_link" class="form-control" value="{{ old('recruiting_google_map_link', isset($contact_us_meta->recruiting_google_map_link) ? $contact_us_meta->recruiting_google_map_link : '') }}">
                @if($errors->has('recruiting_google_map_link'))
                    <p class="help-block">
                        {{ $errors->first('recruiting_google_map_link') }}
                    </p>
                @endif
                
            </div>

            <hr/>

             <label for="designation">Google Location Map</label>

           
            <div class="form-group {{ $errors->has('google_embed_map') ? 'has-error' : '' }}">
                <label for="google_map_location_title">Embed Map *</label>
                <textarea id="google_embed_map" name="google_embed_map" class="form-control">{{ old('google_embed_map', isset($contact_us_meta->google_embed_map) ? $contact_us_meta->google_embed_map : '') }}</textarea>
                
                @if($errors->has('google_embed_map'))
                    <p class="help-block">
                        {{ $errors->first('google_embed_map') }}
                    </p>
                @endif
                
            </div>

            
            
            
            <div>
                <input type="hidden" name="id" value="{{ isset($contact_us_data->id) ? $contact_us_data->id:'0' }}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection