@extends('layouts.admin')
@section('content')
<div class="content">
    <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ $total_contact ?? '0'}}</h3>

                <p>New Contact Form</p>
              </div>
              <div class="icon">
                <i class="fa fa-address-book"></i>
              </div>
              <a href="{{ url('admin/form/contact') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ $total_career ?? '0'}}</h3>

                <p>New Career Form</p>
              </div>
              <div class="icon">
                <i class="fa fa-book"></i>
              </div>
              <a href="{{ url('admin/form/career') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          
        </div>
</div>
@endsection
@section('scripts')
@parent

@endsection