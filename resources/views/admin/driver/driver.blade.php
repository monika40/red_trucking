@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>{{ config('constants.website_name') }} Drivers</b></h4>
   </div>


    <div class="card-body">
        <form action="{{ URL::to('admin/driver/update') }}" class="validate_form" method="POST" enctype="multipart/form-data">
            @csrf
              @php
            $data = isset($driver_data->meta_data) && !empty($driver_data->meta_data) ? json_decode($driver_data->meta_data) : array() ;
            @endphp 

            <hr/>
            <h2>Section 1</h2>
            <hr/>
            <div class="form-group {{ $errors->has('section_one_title') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="section_one_title" name="section_one_title" class="form-control validate[required]" value="{{ old('section_one_title', isset($data->section_one_title) ? $data->section_one_title : '') }}">
                @if($errors->has('section_one_title'))
                    <p class="help-block">
                        {{ $errors->first('section_one_title') }}
                    </p>
                @endif
                
            </div>
           

             <div class="form-group {{ $errors->has('section_one_first_paragraph') ? 'has-error' : '' }}">
                <label for="description">First Paragraph Content *</label>
                <textarea   id="section_one_first_paragraph" name="section_one_first_paragraph" class="form-control validate[required]">{{ old('section_one_first_paragraph', isset($data->section_one_first_paragraph) ? $data->section_one_first_paragraph : '') }}</textarea>
                @if($errors->has('section_one_first_paragraph'))
                    <p class="help-block">
                        {{ $errors->first('section_one_first_paragraph') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('section_one_second_paragraph') ? 'has-error' : '' }}">
                <label for="section_one_second_paragraph">Second Paragraph Content *</label>
                <textarea   id="section_one_second_paragraph" name="section_one_second_paragraph" class="form-control validate[required]">{{ old('section_one_second_paragraph', isset($data->section_one_second_paragraph) ? $data->section_one_second_paragraph : '') }}</textarea>
                @if($errors->has('section_one_second_paragraph'))
                    <p class="help-block">
                        {{ $errors->first('section_one_second_paragraph') }}
                    </p>
                @endif
                
            </div>

             <hr/>
            <h2>Section 2</h2>
            <hr/>
            <div class="form-group {{ $errors->has('section_two_title') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="section_two_title" name="section_two_title" class="form-control validate[required]" value="{{ old('section_two_title', isset($data->section_two_title) ? $data->section_two_title : '') }}">
                @if($errors->has('section_two_title'))
                    <p class="help-block">
                        {{ $errors->first('section_two_title') }}
                    </p>
                @endif
                
            </div>



             <div class="form-group">
                <label for="description">{{ config('constants.website_name') }} Drivers Banners</label>

                <div id="add_more_result">
                    @if(isset($image_data) && !empty($image_data) )
                    @foreach($image_data as $key => $value)
                    
                    <div class="remove_div remove_div{{ $sr }}">
                        <div class="float-right">
                            <button type="button" class="add_more_remove " del_url="{{ URL::to('admin/driver/page_gallery_delete/'.$value->id) }}" page_gallery_id="{{ $value->id ?? '' }}" id="remove_banner{{ $sr }}" counter="{{ $sr }}"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="form-group error_wrapper div_{{ $sr }}">
                            <label for="name">Banner Title *</label>
                            <input class="form-control is_alphabet validate[required]" value="{{ $value->title ?? '' }}" type="text" counter="{{ $sr }}" name="page_gallery[title][{{ $sr }}]" placeholder=" Title">
                        </div>
                        <div class="form-group error_wrapper">
                            <label for="name">Banner Details *</label>
                            <textarea class="form-control validate[required] " placeholder="Details" counter="{{ $sr }}" name="page_gallery[details][{{ $sr }}]">{{ $value->details ?? '' }}</textarea>
                        </div>
                        <input type="hidden" name="page_gallery[gallery_id][{{ $sr }}]" id="page_gallery_id{{ $sr }}" value="{{ $value->id ?? '' }}">
                        <div class="form-group error_wrapper div_{{ $sr }}">
                            <label for="name">Banner Image *</label>
                            @if(isset($value->image) && file_exists($value->image))
                             <img src="{{ URL::to($value->image) }}" height="100" width="100">
                            @endif
                             <br>
                             <p>Note : Image dimension should be within min 901X601 pixels.</p>
                            <input class="" type="file" counter="{{ $sr }}" name="page_gallery[image][{{ $sr }}]" accept="image/*">
                        </div>
                    </div>
                    @php
                    $sr++;
                    @endphp

                     @endforeach
                     @endif

                </div>    
               
               <input type="button" class="add_more_btn btn btn-info" name="add_more" id="add_more_red_driver" value="Add More">
      
            </div>


            <hr/>
            <h2>Section 3</h2>
            <hr/>
             <label> {{ config('constants.website_name') }} Facilities</label>

            <hr/>
                <h3>First Facilities </h3>
                <div class="form-group {{ $errors->has('facilities_first_title') ? 'has-error' : '' }}">
                            <label for="name">Facilities Title *</label>
                            <input class="form-control validate[required]"  type="text" name="facilities_first_title" maxlength="255" value="{{ old('facilities_first_title', isset($data->facilities_first_title) ? $data->facilities_first_title : '') }}">
                            @if($errors->has('facilities_first_title'))
                            <p class="help-block">
                                {{ $errors->first('facilities_first_title') }}
                            </p>
                            @endif
                </div>
                <div class="form-group {{ $errors->has('facilities_first_detail') ? 'has-error' : '' }}">
                            <label for="name">Facilities Detail *</label>
                            <input class="form-control validate[required]"  type="text" name="facilities_first_detail" maxlength="255" value="{{ old('facilities_first_detail', isset($data->facilities_first_detail) ? $data->facilities_first_detail : '') }}">
                            @if($errors->has('facilities_first_detail'))
                            <p class="help-block">
                                {{ $errors->first('facilities_first_detail') }}
                            </p>
                            @endif
                </div>
                 <div class="form-group {{ $errors->has('facilities_first_image') ? 'has-error' : '' }}">
                            <label for="name">Facilities Image *</label>
                             @if(isset($data->facilities_first_image) && file_exists($data->facilities_first_image))
                            <img src="{{ URL::to($data->facilities_first_image) }}" height="100" width="100" style="background: black;">
                            <input type="hidden" name="old_facilities_first_image" value="{{ $data->facilities_first_image }}">
                            @endif
                            <br/>
                            <p>Note : Image dimension should be within min 150X150 pixels.</p>
                            <input class="{{isset($data->facilities_first_image) && file_exists($data->facilities_first_image) ?'' : 'validate[required]'}}"  type="file" name="facilities_first_image" accept="image/*">
                            @if($errors->has('facilities_first_image'))
                            <p class="help-block">
                                {{ $errors->first('facilities_first_image') }}
                            </p>
                            @endif
                </div>
            <hr/>  
                <h3>Second Facilities </h3>
                <div class="form-group {{ $errors->has('facilities_second_title') ? 'has-error' : '' }}">
                            <label for="name">Facilities Title *</label>
                            <input class="form-control validate[required]"  type="text" name="facilities_second_title" maxlength="255" value="{{ old('facilities_second_title', isset($data->facilities_second_title) ? $data->facilities_second_title : '') }}">
                            @if($errors->has('facilities_second_title'))
                            <p class="help-block">
                                {{ $errors->first('facilities_second_title') }}
                            </p>
                            @endif
                </div>
                <div class="form-group {{ $errors->has('facilities_second_detail') ? 'has-error' : '' }}">
                            <label for="name">Facilities Detail *</label>
                            <input class="form-control validate[required]"  type="text" name="facilities_second_detail" maxlength="255" value="{{ old('facilities_second_detail', isset($data->facilities_second_detail) ? $data->facilities_second_detail : '') }}">
                            @if($errors->has('facilities_second_detail'))
                            <p class="help-block">
                                {{ $errors->first('facilities_second_detail') }}
                            </p>
                            @endif
                </div>
                 <div class="form-group {{ $errors->has('facilities_second_image') ? 'has-error' : '' }}">
                            <label for="name">Facilities Image *</label>
                            @if(isset($data->facilities_second_image) && file_exists($data->facilities_second_image))
                            <img src="{{ URL::to($data->facilities_second_image) }}" style="background: black;" height="100" width="100">
                            <input type="hidden" name="old_facilities_second_image" value="{{ $data->facilities_second_image }}">
                            @endif
                            <br/>
                            <p>Note : Image dimension should be within min 150X150 pixels.</p>
                            <input class=" {{isset($data->facilities_second_image) && file_exists($data->facilities_second_image) ?'' : 'validate[required]'}}" type="file" name="facilities_second_image" accept="image/*">
                            @if($errors->has('facilities_second_image'))
                            <p class="help-block">
                                {{ $errors->first('facilities_second_image') }}
                            </p>
                            @endif
                </div>
            <hr/>  

             
                <h3>Third Facilities </h3>
                <div class="form-group {{ $errors->has('facilities_third_title') ? 'has-error' : '' }}">
                            <label for="name">Facilities Title *</label>
                            <input class="form-control validate[required]"  type="text" name="facilities_third_title" maxlength="255" value="{{ old('facilities_third_title', isset($data->facilities_third_title) ? $data->facilities_third_title : '') }}">
                            @if($errors->has('facilities_third_title'))
                            <p class="help-block">
                                {{ $errors->first('facilities_third_title') }}
                            </p>
                            @endif
                </div>
                <div class="form-group {{ $errors->has('facilities_third_detail') ? 'has-error' : '' }}">
                            <label for="name">Facilities Detail *</label>
                            <input class="form-control validate[required]"  type="text" name="facilities_third_detail" maxlength="255" value="{{ old('facilities_third_detail', isset($data->facilities_third_detail) ? $data->facilities_third_detail : '') }}">
                            @if($errors->has('facilities_third_detail'))
                            <p class="help-block">
                                {{ $errors->first('facilities_third_detail') }}
                            </p>
                            @endif
                </div>
                 <div class="form-group {{ $errors->has('facilities_third_image') ? 'has-error' : '' }}">
                            <label for="name">Facilities Image *</label>
                            @if(isset($data->facilities_third_image) && file_exists($data->facilities_third_image))
                            <img src="{{ URL::to($data->facilities_third_image) }}" style="background: black;" height="100" width="100">
                            <input type="hidden" name="old_facilities_third_image" value="{{ $data->facilities_third_image }}">
                            @endif
                            <br/>
                            <p>Note : Image dimension should be within min 150X150 pixels.</p>
                            <input class="{{isset($data->facilities_third_image) && file_exists($data->facilities_third_image) ?'' : 'validate[required]'}}"  type="file" name="facilities_third_image" accept="image/*">
                            @if($errors->has('facilities_third_image'))
                            <p class="help-block">
                                {{ $errors->first('facilities_third_image') }}
                            </p>
                            @endif
                </div>
            

             <hr/>
                <h3>Four Facilities </h3>
                <div class="form-group {{ $errors->has('facilities_four_title') ? 'has-error' : '' }}">
                            <label for="name">Facilities Title *</label>
                            <input class="form-control validate[required]" type="text" name="facilities_four_title" maxlength="255" value="{{ old('facilities_four_title', isset($data->facilities_four_title) ? $data->facilities_four_title : '') }}">
                            @if($errors->has('facilities_four_title'))
                            <p class="help-block">
                                {{ $errors->first('facilities_four_title') }}
                            </p>
                            @endif
                </div>
                <div class="form-group {{ $errors->has('facilities_four_detail') ? 'has-error' : '' }}">
                            <label for="name">Facilities Detail *</label>
                            <input class="form-control validate[required]"  type="text" name="facilities_four_detail" maxlength="255" value="{{ old('section_one_title', isset($data->facilities_four_detail) ? $data->facilities_four_detail : '') }}">
                            @if($errors->has('facilities_four_detail'))
                            <p class="help-block">
                                {{ $errors->first('facilities_four_detail') }}
                            </p>
                            @endif
                </div>
                 <div class="form-group {{ $errors->has('facilities_four_image') ? 'has-error' : '' }}">
                            <label for="name">Facilities Image *</label>
                            @if(isset($data->facilities_four_image) && file_exists($data->facilities_four_image))
                            <img src="{{ URL::to($data->facilities_four_image) }}" style="background: black;" height="100" width="100">
                            <input type="hidden" name="old_facilities_four_image" value="{{ $data->facilities_four_image }}">
                            @endif
                            <br/>
                            <p>Note : Image dimension should be within min 150X150 pixels.</p>
                            <input class="{{isset($data->facilities_four_image) && file_exists($data->facilities_four_image) ?'' : 'validate[required]'}}"  type="file" name="facilities_four_image" accept="image/*">
                            @if($errors->has('facilities_four_image'))
                            <p class="help-block">
                                {{ $errors->first('facilities_four_image') }}
                            </p>
                            @endif
                </div>
            

             <hr/>
                <h3>Five Facilities </h3>
                <div class="form-group {{ $errors->has('facilities_five_title') ? 'has-error' : '' }}">
                            <label for="name">Facilities Title *</label>
                            <input class="form-control validate[required]" type="text" name="facilities_five_title" maxlength="255" value="{{ old('facilities_five_title', isset($data->facilities_five_title) ? $data->facilities_five_title : '') }}">
                            @if($errors->has('facilities_five_title'))
                            <p class="help-block">
                                {{ $errors->first('facilities_five_title') }}
                            </p>
                            @endif
                </div>
                <div class="form-group {{ $errors->has('facilities_five_detail') ? 'has-error' : '' }}">
                            <label for="name">Facilities Detail *</label>
                            <input class="form-control validate[required]" type="text" name="facilities_five_detail" maxlength="255" value="{{ old('facilities_five_detail', isset($data->facilities_five_detail) ? $data->facilities_five_detail : '') }}">
                            @if($errors->has('facilities_five_detail'))
                            <p class="help-block">
                                {{ $errors->first('facilities_five_detail') }}
                            </p>
                            @endif
                </div>
                 <div class="form-group {{ $errors->has('facilities_five_image') ? 'has-error' : '' }}">
                            <label for="name">Facilities Image *</label>
                            @if(isset($data->facilities_five_image) && file_exists($data->facilities_five_image))
                            <img src="{{ URL::to($data->facilities_five_image) }}" style="background: black;" height="100" width="100">
                            <input type="hidden" name="old_facilities_five_image" value="{{ $data->facilities_five_image }}">
                            @endif
                            <br/>
                            <p>Note : Image dimension should be within min 150X150 pixels.</p>
                            <input class=" {{isset($data->facilities_five_image) && file_exists($data->facilities_five_image) ?'' : 'validate[required]'}}"  type="file" name="facilities_five_image" accept="image/*">
                            @if($errors->has('facilities_five_image'))
                            <p class="help-block">
                                {{ $errors->first('facilities_five_image') }}
                            </p>
                            @endif
                </div>
            
             <hr/>
                <h3>Six Facilities </h3>
                <div class="form-group {{ $errors->has('facilities_six_title') ? 'has-error' : '' }}">
                            <label for="name">Facilities Title *</label>
                            <input class="form-control validate[required]" type="text" name="facilities_six_title" maxlength="255" value="{{ old('facilities_six_title', isset($data->facilities_six_title) ? $data->facilities_six_title : '') }}">
                            @if($errors->has('facilities_six_title'))
                            <p class="help-block">
                                {{ $errors->first('facilities_six_title') }}
                            </p>
                            @endif
                </div>
                <div class="form-group {{ $errors->has('facilities_six_detail') ? 'has-error' : '' }}">
                            <label for="name">Facilities Detail *</label>
                            <input class="form-control validate[required]" type="text" name="facilities_six_detail" maxlength="255" value="{{ old('section_one_title', isset($data->facilities_six_detail) ? $data->facilities_six_detail : '') }}">
                            @if($errors->has('facilities_six_detail'))
                            <p class="help-block">
                                {{ $errors->first('facilities_six_detail') }}
                            </p>
                            @endif
                </div>
                 <div class="form-group {{ $errors->has('facilities_six_image') ? 'has-error' : '' }}">
                            <label for="name">Facilities Image *</label>
                            @if(isset($data->facilities_six_image) && file_exists($data->facilities_six_image))
                            <img src="{{ URL::to($data->facilities_six_image) }}" style="background: black;" height="100" width="100">
                            <input type="hidden" name="old_facilities_six_image" value="{{ $data->facilities_six_image }}">
                            @endif
                            <br/>
                            <p>Note : Image dimension should be within min 150X150 pixels.</p>
                            <input class="{{isset($data->facilities_six_image) && file_exists($data->facilities_six_image) ?'' : 'validate[required]'}}" type="file" name="facilities_six_image" accept="image/*">
                            @if($errors->has('facilities_six_image'))
                            <p class="help-block">
                                {{ $errors->first('facilities_six_image') }}
                            </p>
                            @endif
                </div>
        

            <hr/>
            <h2>Section 4</h2>
            <hr/>
            <div class="form-group {{ $errors->has('section_four_title') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="section_four_title" name="section_four_title" class="form-control validate[required]" value="{{ old('section_four_title', isset($data->section_four_title) ? $data->section_four_title : '') }}">
                @if($errors->has('section_four_title'))
                    <p class="help-block">
                        {{ $errors->first('section_four_title') }}
                    </p>
                @endif
                
            </div>
           

             <div class="form-group {{ $errors->has('section_four_first_paragraph') ? 'has-error' : '' }}">
                <label for="section_four_first_paragraph">First Paragraph Content *</label>
                <textarea   id="section_four_first_paragraph" name="section_four_first_paragraph" class="form-control validate[required]">{{ old('section_four_first_paragraph', isset($data->section_four_first_paragraph) ? $data->section_four_first_paragraph : '') }}</textarea>
                @if($errors->has('section_four_first_paragraph'))
                    <p class="help-block">
                        {{ $errors->first('section_four_first_paragraph') }}
                    </p>
                @endif
                
            </div>
            <div class="form-group {{ $errors->has('section_four_second_paragraph') ? 'has-error' : '' }}">
                <label for="section_four_second_paragraph">Second Paragraph Content *</label>
                <textarea   id="section_four_second_paragraph" name="section_four_second_paragraph" class="form-control validate[required]">{{ old('section_four_second_paragraph', isset($data->section_four_second_paragraph) ? $data->section_four_second_paragraph : '') }}</textarea>
                @if($errors->has('section_four_second_paragraph'))
                    <p class="help-block">
                        {{ $errors->first('section_four_second_paragraph') }}
                    </p>
                @endif
                
            </div>
            <label>Paragraph Points</label>
             <div class="form-group {{ $errors->has('section_four_first_point') ? 'has-error' : '' }}">
                <input type="text" id="section_four_first_point" name="section_four_first_point"  placeholder="First Point*" class="form-control validate[required]" value="{{ old('section_four_first_point', isset($data->section_four_first_point) ? $data->section_four_first_point : '') }}" maxlength="255">
                @if($errors->has('section_four_first_point'))
                    <p class="help-block">
                        {{ $errors->first('section_four_first_point') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('section_four_second_point') ? 'has-error' : '' }}">
                <input type="text" id="section_four_second_point" name="section_four_second_point"  placeholder="Second Point*" class="form-control validate[required]" value="{{ old('section_four_second_point', isset($data->section_four_second_point) ? $data->section_four_second_point : '') }}" maxlength="255">
                @if($errors->has('section_four_second_point'))
                    <p class="help-block">
                        {{ $errors->first('section_four_second_point') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('section_four_third_point') ? 'has-error' : '' }}">
                <input type="text" id="section_four_third_point" name="section_four_third_point"  placeholder="Third Point*" class="form-control validate[required]" value="{{ old('section_four_third_point', isset($data->section_four_first_point) ? $data->section_four_third_point : '') }}" maxlength="255">
                @if($errors->has('section_four_third_point'))
                    <p class="help-block">
                        {{ $errors->first('section_four_third_point') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('section_four_four_point') ? 'has-error' : '' }}">
                <input type="text" id="section_four_four_point" name="section_four_four_point"  placeholder="Four Point" class="form-control" value="{{ old('section_four_four_point', isset($data->section_four_four_point) ? $data->section_four_four_point : '') }}" maxlength="255">
                @if($errors->has('section_four_four_point'))
                    <p class="help-block">
                        {{ $errors->first('section_four_four_point') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('section_four_five_point') ? 'has-error' : '' }}">
                <input type="text" id="section_four_five_point" name="section_four_five_point"  placeholder="Five Point" class="form-control " value="{{ old('section_four_five_point', isset($data->section_four_five_point) ? $data->section_four_five_point : '') }}" maxlength="255">
                @if($errors->has('section_four_five_point'))
                    <p class="help-block">
                        {{ $errors->first('section_four_five_point') }}
                    </p>
                @endif
            </div>


          


            
            <div>
                <input type="hidden" name="meta_id" value="{{ isset($driver_data->id) ? $driver_data->id : 0 }}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>


@endsection
@section('scripts')
@parent

@endsection
