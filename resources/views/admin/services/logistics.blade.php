@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        <h4><b>Logistics</b></h4>
    </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/services/updatelogistics') }}" class="validate_form" method="POST" enctype="multipart/form-data">
            @csrf
            @php
            $data = isset($logistics_data->meta_data) && !empty($logistics_data->meta_data) ? json_decode($logistics_data->meta_data) : array() ;
            @endphp 
            <hr/>
            <h2>Section 1</h2>
            <hr/>

            <div class="form-group {{ $errors->has('section_one_title') ? 'has-error' : '' }}">
                <label for="name">First Title*</label>
                <input type="text" id="section_one_title" name="section_one_title" class="form-control validate[required]" value="{{ old('section_one_title', isset($data->section_one_title) ? $data->section_one_title : '') }}">
                @if($errors->has('section_one_title'))
                    <p class="help-block">
                        {{ $errors->first('section_one_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_one_first_paragraphp') ? 'has-error' : '' }}">
                <label for="section_one_first_paragraphp">First Paragraph Content *</label>
                <textarea id="section_one_first_paragraphp" name="section_one_first_paragraphp" class="form-control validate[required] ">{{ old('section_one_first_paragraphp', isset($data->section_one_first_paragraphp) ? $data->section_one_first_paragraphp : '') }}</textarea>
                @if($errors->has('section_one_first_paragraphp'))
                    <p class="help-block">
                        {{ $errors->first('section_one_first_paragraphp') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_one_second_paragraphp') ? 'has-error' : '' }}">
                <label for="section_one_second_paragraphp">Second Paragraph Content *</label>
                <textarea id="section_one_second_paragraphp" name="section_one_second_paragraphp" class="form-control validate[required] ">{{ old('section_one_second_paragraphp', isset($data->section_one_second_paragraphp) ? $data->section_one_second_paragraphp : '') }}</textarea>
                @if($errors->has('section_one_second_paragraphp'))
                    <p class="help-block">
                        {{ $errors->first('section_one_second_paragraphp') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_one_second_title') ? 'has-error' : '' }}">
                <label for="name">Second Title*</label>
                <input type="text" id="section_one_second_title" name="section_one_second_title" class="form-control validate[required]" value="{{ old('section_one_second_title', isset($data->section_one_second_title) ? $data->section_one_second_title : '') }}">
                @if($errors->has('section_one_second_title'))
                    <p class="help-block">
                        {{ $errors->first('section_one_second_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_one_second_description') ? 'has-error' : '' }}">
                <label for="section_one_second_description">Second Description *</label>
                <textarea id="section_one_second_description" name="section_one_second_description" class="form-control validate[required] ">{{ old('section_one_second_description', isset($data->section_one_second_description) ? $data->section_one_second_description : '') }}</textarea>
                @if($errors->has('section_one_second_description'))
                    <p class="help-block">
                        {{ $errors->first('section_one_second_description') }}
                    </p>
                @endif
                
            </div>


            <hr/>
            <h2>Section 2</h2>
            <hr/>

            <div class="form-group {{ $errors->has('section_two_first_title') ? 'has-error' : '' }}">
                <label for="name">First Title*</label>
                <input type="text" id="section_two_title" name="section_two_first_title" class="form-control validate[required]" value="{{ old('section_two_first_title', isset($data->section_two_first_title) ? $data->section_two_first_title : '') }}">
                @if($errors->has('section_two_first_title'))
                    <p class="help-block">
                        {{ $errors->first('section_two_first_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_two_first_description') ? 'has-error' : '' }}">
                <label for="section_two_first_description">First Paragraph Content *</label>
                <textarea id="section_two_first_description" name="section_two_first_description" class="form-control validate[required] ">{{ old('section_two_first_description', isset($data->section_two_first_description) ? $data->section_two_first_description : '') }}</textarea>
                @if($errors->has('section_two_first_description'))
                    <p class="help-block">
                        {{ $errors->first('section_two_first_description') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_two_second_title') ? 'has-error' : '' }}">
                <label for="name">Second Title*</label>
                <input type="text" id="section_two_second_title" name="section_two_second_title" class="form-control validate[required]" value="{{ old('section_two_second_title', isset($data->section_two_second_title) ? $data->section_two_second_title : '') }}">
                @if($errors->has('section_two_second_title'))
                    <p class="help-block">
                        {{ $errors->first('section_two_second_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_two_second_description') ? 'has-error' : '' }}">
                <label for="section_two_second_description">Second Paragraph Content *</label>
                <textarea id="section_two_second_description" name="section_two_second_description" class="form-control validate[required] ">{{ old('section_two_second_description', isset($data->section_two_second_description) ? $data->section_two_second_description : '') }}</textarea>
                @if($errors->has('section_two_second_description'))
                    <p class="help-block">
                        {{ $errors->first('section_two_second_description') }}
                    </p>
                @endif
                
            </div>

            <hr/>
            <h2>Section 3</h2>
            <hr/>
            
            <div class="form-group {{ $errors->has('section_third_first_title') ? 'has-error' : '' }}">
                <label for="name">First Title*</label>
                <input type="text" id="section_two_title" name="section_third_first_title" class="form-control validate[required]" value="{{ old('section_third_first_title', isset($data->section_third_first_title) ? $data->section_third_first_title : '') }}">
                @if($errors->has('section_third_first_title'))
                    <p class="help-block">
                        {{ $errors->first('section_third_first_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_three_first_description') ? 'has-error' : '' }}">
                <label for="section_three_first_description">First Paragraph Content *</label>
                <textarea id="section_three_first_description" name="section_three_first_description" class="form-control validate[required] ">{{ old('section_three_first_description', isset($data->section_three_first_description) ? $data->section_three_first_description : '') }}</textarea>
                @if($errors->has('section_three_first_description'))
                    <p class="help-block">
                        {{ $errors->first('section_three_first_description') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_three_second_title') ? 'has-error' : '' }}">
                <label for="name">Second Title*</label>
                <input type="text" id="section_three_second_title" name="section_three_second_title" class="form-control validate[required]" value="{{ old('section_three_second_title', isset($data->section_three_second_title) ? $data->section_three_second_title : '') }}">
                @if($errors->has('section_three_second_title'))
                    <p class="help-block">
                        {{ $errors->first('section_three_second_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_three_second_description') ? 'has-error' : '' }}">
                <label for="section_three_second_description">Second Paragraph Content *</label>
                <textarea id="section_three_second_description" name="section_three_second_description" class="form-control validate[required] ">{{ old('section_three_second_description', isset($data->section_three_second_description) ? $data->section_three_second_description : '') }}</textarea>
                @if($errors->has('section_three_second_description'))
                    <p class="help-block">
                        {{ $errors->first('section_three_second_description') }}
                    </p>
                @endif
                
            </div>

             <div class="form-group {{ $errors->has('section_third_third_title') ? 'has-error' : '' }}">
                <label for="name">Third Title*</label>
                <input type="text" id="section_third_third_title" name="section_third_third_title" class="form-control validate[required]" value="{{ old('section_third_third_title', isset($data->section_third_third_title) ? $data->section_third_third_title : '') }}">
                @if($errors->has('section_third_third_title'))
                    <p class="help-block">
                        {{ $errors->first('section_third_third_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_third_third_description') ? 'has-error' : '' }}">
                <label for="section_third_third_description">Third Paragraph Content *</label>
                <textarea id="section_third_third_description" name="section_third_third_description" class="form-control validate[required] ">{{ old('section_third_third_description', isset($data->section_third_third_description) ? $data->section_third_third_description : '') }}</textarea>
                @if($errors->has('section_third_third_description'))
                    <p class="help-block">
                        {{ $errors->first('section_third_third_description') }}
                    </p>
                @endif
                
            </div>


           

            <div class="form-group {{ $errors->has('section_three_image') ? 'has-error' : '' }}">
                            <label for="name"> Image *</label>
                             @if(isset($data->section_three_image) && file_exists($data->section_three_image))
                            <img src="{{ URL::to($data->section_three_image) }}" height="100" width="100" style="background: black;">
                            <input type="hidden" name="old_section_three_image" value="{{ $data->section_three_image }}">
                            @endif
                            <br/>
                            <p>Note : Image dimension should be within min 610X526 pixels.</p>
                            <input class="{{isset($data->section_three_image) && file_exists($data->section_three_image) ?'' : 'validate[required]'}}"  type="file" name="section_three_image" accept="image/*">
                            @if($errors->has('section_three_image'))
                            <p class="help-block">
                                {{ $errors->first('section_three_image') }}
                            </p>
                            @endif
                </div>

            
            
            <div>
                <input type="hidden" name="id" value="{{ isset($logistics_data->id) ? $logistics_data->id:'' }}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection