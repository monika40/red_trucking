@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        <h4><b>Warehousing</b></h4>
    </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/services/updateswarehousing') }}" class="validate_form" method="POST" enctype="multipart/form-data">
            @csrf
            @php
            $data = isset($warehousing_data->meta_data) && !empty($warehousing_data->meta_data) ? json_decode($warehousing_data->meta_data) : array() ;
            @endphp 
            <hr/>
            <h2>Section 1</h2>
            <hr/>

            <div class="form-group {{ $errors->has('section_one_title') ? 'has-error' : '' }}">
                <label for="name">First Title*</label>
                <input type="text" id="section_one_title" name="section_one_title" class="form-control validate[required]" value="{{ old('section_one_title', isset($data->section_one_title) ? $data->section_one_title : '') }}">
                @if($errors->has('section_one_title'))
                    <p class="help-block">
                        {{ $errors->first('section_one_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_one_first_paragraphp') ? 'has-error' : '' }}">
                <label for="section_one_first_paragraphp">First Paragraph Content *</label>
                <textarea id="section_one_first_paragraphp" name="section_one_first_paragraphp" class="form-control validate[required] ">{{ old('section_one_first_paragraphp', isset($data->section_one_first_paragraphp) ? $data->section_one_first_paragraphp : '') }}</textarea>
                @if($errors->has('section_one_first_paragraphp'))
                    <p class="help-block">
                        {{ $errors->first('section_one_first_paragraphp') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_one_second_paragraphp') ? 'has-error' : '' }}">
                <label for="section_one_second_paragraphp">Second Paragraph Content *</label>
                <textarea id="section_one_second_paragraphp" name="section_one_second_paragraphp" class="form-control validate[required] ">{{ old('section_one_second_paragraphp', isset($data->section_one_second_paragraphp) ? $data->section_one_second_paragraphp : '') }}</textarea>
                @if($errors->has('section_one_second_paragraphp'))
                    <p class="help-block">
                        {{ $errors->first('section_one_second_paragraphp') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_one_second_title') ? 'has-error' : '' }}">
                <label for="name">Second Title*</label>
                <input type="text" id="section_one_second_title" name="section_one_second_title" class="form-control validate[required]" value="{{ old('section_one_second_title', isset($data->section_one_second_title) ? $data->section_one_second_title : '') }}">
                @if($errors->has('section_one_second_title'))
                    <p class="help-block">
                        {{ $errors->first('section_one_second_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_one_second_first_paragraphp') ? 'has-error' : '' }}">
                <label for="section_one_second_first_paragraphp">First Paragraph Content *</label>
                <textarea id="section_one_second_first_paragraphp" name="section_one_second_first_paragraphp" class="form-control validate[required] ">{{ old('section_one_second_first_paragraphp', isset($data->section_one_second_first_paragraphp) ? $data->section_one_second_first_paragraphp : '') }}</textarea>
                @if($errors->has('section_one_second_first_paragraphp'))
                    <p class="help-block">
                        {{ $errors->first('section_one_second_first_paragraphp') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_one_second_second_paragraphp') ? 'has-error' : '' }}">
                <label for="section_one_second_second_paragraphp">Second Paragraph Content *</label>
                <textarea id="section_one_second_second_paragraphp" name="section_one_second_second_paragraphp" class="form-control validate[required] ">{{ old('section_one_second_second_paragraphp', isset($data->section_one_second_second_paragraphp) ? $data->section_one_second_second_paragraphp : '') }}</textarea>
                @if($errors->has('section_one_second_second_paragraphp'))
                    <p class="help-block">
                        {{ $errors->first('section_one_second_second_paragraphp') }}
                    </p>
                @endif
                
            </div>


            <hr/>
            <h2>Section 2</h2>
            <hr/>

            <div class="form-group {{ $errors->has('section_two_title') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="section_two_title" name="section_two_title" class="form-control validate[required]" value="{{ old('section_two_title', isset($data->section_two_title) ? $data->section_two_title : '') }}">
                @if($errors->has('section_two_title'))
                    <p class="help-block">
                        {{ $errors->first('section_two_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_two_first_paragraphp') ? 'has-error' : '' }}">
                <label for="section_two_first_paragraphp">First Paragraph Content *</label>
                <textarea id="section_two_first_paragraphp" name="section_two_first_paragraphp" class="form-control validate[required] ">{{ old('section_two_first_paragraphp', isset($data->section_two_first_paragraphp) ? $data->section_two_first_paragraphp : '') }}</textarea>
                @if($errors->has('section_two_first_paragraphp'))
                    <p class="help-block">
                        {{ $errors->first('section_two_first_paragraphp') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_two_second_paragraphp') ? 'has-error' : '' }}">
                <label for="section_two_second_paragraphp">Second Paragraph Content *</label>
                <textarea id="section_two_second_paragraphp" name="section_two_second_paragraphp" class="form-control validate[required] ">{{ old('section_two_second_paragraphp', isset($data->section_two_second_paragraphp) ? $data->section_two_second_paragraphp : '') }}</textarea>
                @if($errors->has('section_two_second_paragraphp'))
                    <p class="help-block">
                        {{ $errors->first('section_two_second_paragraphp') }}
                    </p>
                @endif
                
            </div>

            <hr/>
            <h2>Section 3</h2>
            <hr/>
            <div class="form-group {{ $errors->has('section_three_title') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="section_three_title" name="section_three_title" class="form-control validate[required]" value="{{ old('section_three_title', isset($data->section_three_title) ? $data->section_three_title : '') }}">
                @if($errors->has('section_three_title'))
                    <p class="help-block">
                        {{ $errors->first('section_three_title') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('section_three_description') ? 'has-error' : '' }}">
                <label for="section_three_description">Description *</label>
                <textarea id="section_three_description" name="section_three_description" class="form-control validate[required] ">{{ old('section_three_description', isset($data->section_three_description) ? $data->section_three_description : '') }}</textarea>
                @if($errors->has('section_three_description'))
                    <p class="help-block">
                        {{ $errors->first('section_three_description') }}
                    </p>
                @endif
                
            </div>


            
            <label>Benefits </label>
            <div class="form-group {{ $errors->has('section_three_benefit_title') ? 'has-error' : '' }}">
                <label for="name">Title*</label>
                <input type="text" id="section_three_benefit_title" name="section_three_benefit_title" class="form-control validate[required]" value="{{ old('section_three_benefit_title', isset($data->section_three_benefit_title) ? $data->section_two_title : '') }}">
                @if($errors->has('section_three_benefit_title'))
                    <p class="help-block">
                        {{ $errors->first('section_three_benefit_title') }}
                    </p>
                @endif
                
            </div>
            @php
            $benefits = isset($data->section_three_benefits) && !empty($data->section_three_benefits) ? json_decode($data->section_three_benefits) : array() ;
            @endphp
            @if(is_array($benefits) && sizeof($benefits) == 0 )
            <div class="form-group benefit_remove_div  {{ $errors->has('section_three_benefits') ? 'has-error' : '' }}">
                <label for="name">Benefit*</label>
                <input type="text" id="section_three_features" name="section_three_benefits[]" class="form-control validate[required]" value="">
            </div>
            @endif

            <div id="section_two_feature_result">
                @foreach($benefits as $p_key => $value)
                <div class="form-group benefit_remove_div remove_div{{$p_key+1}}" >
                    <label for="name"> Benefit*</label>
                    <button type="button" class="btn btn-danger   add_more_remove float-right" counter="{{$p_key+1}}"> Remove</button>
                    <input type="text"  name="section_three_benefits[]" class="form-control validate[required]" value="{{ $value ?? '' }}">
                </div>
                @endforeach
            </div>
            @if($errors->has('section_three_benefits[]'))
                    <p class="help-block">
                        {{ $errors->first('section_three_benefits[]') }}
                    </p>
            @endif

            <button class="btn btn-info section_two_benefit" type="button">Add More Benefits</button>
            <br />
            <br />
            

            <div class="form-group {{ $errors->has('section_three_image') ? 'has-error' : '' }}">
                            <label for="name"> Image *</label>
                             @if(isset($data->section_three_image) && file_exists($data->section_three_image))
                            <img src="{{ URL::to($data->section_three_image) }}" height="100" width="100" style="background: black;">
                            <input type="hidden" name="old_section_three_image" value="{{ $data->section_three_image }}">
                            @endif
                            <br/>
                            <p>Note : Image dimension should be within min 694X526 pixels.</p>
                            <input class="{{isset($data->section_three_image) && file_exists($data->section_three_image) ?'' : 'validate[required]'}}"  type="file" name="section_three_image" accept="image/*">
                            @if($errors->has('section_three_image'))
                            <p class="help-block">
                                {{ $errors->first('section_three_image') }}
                            </p>
                            @endif
                </div>

            
            
            <div>
                <input type="hidden" name="id" value="{{ isset($warehousing_data->id) ? $warehousing_data->id:'' }}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection