@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>{{ config('constants.website_name') }} Banners</b></h4>
   </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/banner/update') }}" class="validate_form" method="POST" enctype="multipart/form-data">
            @csrf
          

             <div class="form-group">
            
                <div id="add_more_result">
                    @if(isset($image_data) && !empty($image_data) )
                    {{ pa($image_data) }}
                    @foreach($image_data as $key => $value) 
                    
                    <div class="remove_div remove_div{{ $sr }}">
                        <div class="float-right">
                            <button type="button" class="add_more_remove " del_url="{{ URL::to('admin/banner/delete/'.$value->id) }}" banner_id="{{ $value->id ?? '' }}" id="remove_banner{{ $sr }}" counter="{{ $sr }}"><i class="fa fa-times"></i></button>
                        </div>
                        <div class="form-group error_wrapper div_{{ $sr }}">
                            <label for="name">Banner Title *</label>
                            <input class="form-control is_alphabet validate[required]" value="{{ $value->title ?? '' }}" type="text" counter="{{ $sr }}" name="page_gallery[title][{{ $sr }}]" placeholder=" Title">
                        </div>
                        <div class="form-group error_wrapper">
                            <label for="name">Banner Details *</label>
                            <textarea class="form-control validate[required] " placeholder="Details" counter="{{ $sr }}" name="page_gallery[details][{{ $sr }}]">{{ $value->details ?? '' }}</textarea>
                        </div>
                        <input type="hidden" name="page_gallery[banner_id][{{ $sr }}]" id="page_gallery_id{{ $sr }}" value="{{ $value->id ?? '' }}">

                        <div class="form-group error_wrapper div_{{ $sr }}">
                            <label for="name">Banner Background Image *</label>
                            @if(isset($value->background_image) && file_exists($value->background_image))
                             <img src="{{ URL::to($value->background_image) }}" height="100" width="100">
                            @endif
                             <br>
                            <input class="" type="file" counter="{{ $sr }}" name="page_gallery[background_image][{{ $sr }}]" accept="image/*">
                        </div>

                        <div class="form-group error_wrapper div_{{ $sr }}">
                            <label for="name">Logo Banner Title *</label>
                            <input class="form-control is_alphabet" value="{{ $value->logo_title ?? '' }}" type="text" counter="{{ $sr }}" name="page_gallery[logo_title][{{ $sr }}]" placeholder=" Title">
                        </div>

                         <div class="form-group error_wrapper div_{{ $sr }}">
                            <label for="name">Logo Banner Image *</label>
                            @if(isset($value->logo_image) && !empty($value->logo_image) && file_exists($value->logo_image))
                             <img src="{{ URL::to($value->logo_image) }}" height="100" width="100" style="background-color: #212529;">
                            @endif
                             <br>
                            <input class="" type="file" counter="{{ $sr }}" name="page_gallery[logo_image][{{ $sr }}]" accept="image/*">
                        </div>

                        <div class="form-group error_wrapper div_{{ $sr }}">
                            <label for="name">First Banner Image *</label>
                            @if(isset($value->image) && file_exists($value->image))
                             <img src="{{ URL::to($value->image) }}" height="100" width="100">
                            @endif
                             <br>
                            <input class="" type="file" counter="{{ $sr }}" name="page_gallery[image][{{ $sr }}]" accept="image/*">
                        </div>

                         <div class="form-group error_wrapper div_{{ $sr }}">
                            <label for="name">Second Banner Image </label>
                            @if(isset($value->second_image) && file_exists($value->second_image))
                             <img src="{{ URL::to($value->second_image) }}" height="100" width="100">
                            @endif
                             <br>
                            <input class="" type="file" counter="{{ $sr }}" name="page_gallery[second_image][{{ $sr }}]" accept="image/*">
                        </div>

                         <div class="form-group error_wrapper div_{{ $sr }}">
                            <label for="name">Third Banner Image </label>
                            @if(isset($value->third_image) && file_exists($value->third_image))
                             <img src="{{ URL::to($value->third_image) }}" height="100" width="100">
                            @endif
                             <br>
                            <input class="" type="file" counter="{{ $sr }}" name="page_gallery[third_image][{{ $sr }}]" accept="image/*">
                        </div>

                        
                    </div>
                    @php
                    $sr++;
                    @endphp

                     @endforeach
                     @endif

                </div>    
               
               <input type="button" class="add_more_btn btn btn-info" name="add_more" id="add_more_red_banner" value="Add Banner">
      
            </div>


          


            
            <div>
                <input type="hidden" name="id" value="{{ isset($data->id) ? $data->id:'' }}">
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>


@endsection
@section('scripts')
@parent

@endsection
