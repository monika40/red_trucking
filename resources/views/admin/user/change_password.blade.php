@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
       <h4><b>Change Password</b></h4>
   </div>

    <div class="card-body">
        <form action="{{ URL::to('admin/update_password') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('old_password') ? 'has-error' : '' }}">
                <label for="name">Old Password*</label>
                <input type="password" id="old_password" name="old_password" class="form-control" value="{{ old('old_password','') }}" placeholder="Enter Old Password">
                @if($errors->has('old_password'))
                    <p class="help-block">
                        {{ $errors->first('old_password') }}
                    </p>
                @endif
                
            </div>

             <div class="form-group {{ $errors->has('new_password') ? 'has-error' : '' }}">
                <label for="name">New Password*</label>
                <input type="password" id="new_password" name="new_password" class="form-control" value="{{ old('new_password','') }}" placeholder="Enter New Password">
                @if($errors->has('new_password'))
                    <p class="help-block">
                        {{ $errors->first('new_password') }}
                    </p>
                @endif
                
            </div>

            <div class="form-group {{ $errors->has('new_password') ? 'has-error' : '' }}">
                <label for="name">Confirm Password*</label>
                <input type="password" id="confirm_password" name="confirm_password" class="form-control" value="{{ old('confirm_password','') }}" placeholder="Enter Confirm Password">
                @if($errors->has('confirm_password'))
                    <p class="help-block">
                        {{ $errors->first('confirm_password') }}
                    </p>
                @endif
                
            </div>
         
            
            <div>
                <input class="btn btn-success" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection