<!Doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
        <link href="{{ URL::to(config('constants.website_favicon_image')) }}" rel="icon" />
     <title>{{ isset($page_title) ? $page_title.'|'.config('constants.website_name') :config('constants.website_name') }}</title>
     <link rel="stylesheet" href="{{ asset('css/frontend/main.css') }}">

<style>
  *{margin:0px; padding:0px; -webkit-box-sizing:border-box;
  -ms-box-sizing:border-box; -moz-box-sizing:border-box; box-sizing:border-box;}

.error-page{display: -ms-flexbox;display: flex;-ms-flex-wrap: wrap;
  flex-wrap: wrap;-webkit-box-align: center;-ms-flex-align: center;
  align-items: center; -webkit-box-pack: center;  
  justify-content: center; height: 100vh; text-align:center;}

.hgroup h1{font-size:250px; line-height:230px;}
.hgroup h1 .fs01{color:#ff0000;display:inline-block; margin:0 12px;}
.hgroup h1 .fs02{position:relative; display:inline-block; -webkit-transform: rotate(180deg);
-moz-transform: rotate(180deg); -ms-transform: rotate(180deg); transform: rotate(180deg);}
.hgroup h3{font-size:55px; font-family: 'Roboto'; text-transform: uppercase; font-weight: bold;
margin: 15px 0 0 0; line-height:1;}


.error-page{position:relative;}
.error-page:before ,.error-page:after{content:''; position:absolute; width:585px; height:500px;
background-repeat:no-repeat; z-index:-1;}

.error-page:before{background-image:url({{ URL::to(asset('images/frontend/right-strip.png')) }} );right:0; top:0;}
.error-page:after{background-image:url({{ URL::to(asset('images/frontend/left-strip.png')) }} ); left:0; bottom:0;}


.error-page p{color: #000; font-size:18px;}
.site-btn{background-color: #f20003; color: #fff;padding: 12px 30px; margin-top:30px; display: inline-block;}
.site-btn img{margin-right:15px;}
.site-btn:hover{color:#fff;}

.container{width:1024px; margin:0 auto;}


</style>

</head>

<body>

<div class="error-page">
  <div id="container" class="container">
      <!-- ###################### -->
      <div class="hgroup">
        <h1>4<span class="fs01">0</span><span class="fs02">4</span></h1>
        <h3><span>Page Not Found</span></h3>
      </div>
      <p>The page you're looking for doesn't exist or has been moved</p>
      <p><a href="{{ URL::to('/') }}" class="site-btn"><img src="https://clientapp.brandmydream.com/red_trucking/images/frontend/left-arrow.png" alt=""> GO TO HOMEPAGE</a></p>
    <!-- ###### -->
  </div>
</div>

</body>