<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link href="{{ URL::to(config('constants.website_favicon_image')) }}" rel="icon" />
    <title>{{ isset($page_title) ? $page_title.'|'.config('constants.website_name') :config('constants.website_name') }}</title>
    <link rel="stylesheet" href="{{ asset('css/frontend/main.css') }}">

    @yield('styles')

    <script type="text/javascript">
      var base_url = '{{ env('APP_URL') }}';
    </script>

<style>
    .redhome_header{background-color:#000;}
    .redhome_header > .navbar-nav > .nav-item > a{color:#fff;}
</style>

</head>

<body>
    
@include('partials.frontend.header')
<section class="thank-you-con">
    <div class="container">
        <div class="thank-you-page">
            <h1><span class="thx01">Thank</span> <span class="thx02">You</span></h1>
            <p>{!! $message !!}</p>
            <a href="{{ URL::to('/') }}" class="site-btn">Go Back</a>
        </div>
    </div>
</section>

<!-- Glabol js -->
    <script src="{{ asset('js/frontend/jquery.min.js') }}"></script>
    <script src="{{ asset('js/frontend/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/frontend/common.js?reload=true') }}"></script>
    <script src="{{ asset('js/frontend/custom.js?reload=true') }}"></script>

</body>
</html>
