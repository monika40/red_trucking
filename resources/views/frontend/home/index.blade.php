@extends('layouts.frontend')
@section('content')
 

<!--Home Sliders-->
 @if(isset($banner_data) && !empty($banner_data) )
    <section class="home_banner">
        <div id="redTruckingCarousel" class="carousel slide carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators">
                @foreach($banner_data as $key => $value) 
                <li data-target="#redTruckingCarousel" data-slide-to="{{ $key}}" class="{{ $key==0?'active':''}}"></li>
                @endforeach
            </ol>
            <div class="carousel-inner">
                @foreach($banner_data as $key => $value)

                @if(!empty($value->background_image) && !empty($value->image) && !empty($value->second_image) && empty($value->third_image) && !empty($value->logo_title) && !empty($value->logo_image) )
                <div class="carousel-item {{ $key==0?'active':''}}">
                    <img src="{{ URL::to($value->background_image) }}" class="d-block w-100" alt="3">
                    <div class="redtruck_caption">
                        <div class="row">
                            <div class="col-6">
                                <div class="carousel_content caro-cont1">
                                    <h2 class="wow slideInLeft">{{ $value->title ?? '' }}</h2>
                                    <p class="wow slideInDown"> @php echo $value->details ?? ''; @endphp</p>
                                    <a href="{{ URL::to('contact') }}" class="redbutton wow flipInX">Transport with best</a>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="carousel_image1">
                                    <div class=" banner1-rightimg1  wow fadeInDown">
                                        <img src="{{ URL::to($value->image) }}" class="img-fluid">
                                    </div>
                                    <div class="carousel_cowan wow slideInRight" style="visibility: visible; animation-name: slideInRight;">
                                        <h6 class="mr-3">{{ $value->logo_title ?? '' }}</h6>
                                        <img src="{{ URL::to($value->logo_image) }}" class="img-fluid">
                                    </div>

                                    <div class="banner1-rightimg2  wow slideInRight">
                                        <img src="{{ URL::to($value->second_image) }}" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if(!empty($value->background_image) && !empty($value->image) && empty($value->second_image) && empty($value->third_image)  )
                <div class="carousel-item {{ $key==0?'active':''}}">
                    <img src="{{ URL::to($value->background_image) }}" class="d-block w-100" alt="2">
                    <div class="redtruck_caption">
                        <div class="row">
                            <div class="col-6">
                                <div class="carousel_content caro-cont2">
                                    <h2 class="wow slideInLeft">{{ $value->title ?? '' }}</h2>
                                    <p class="wow slideInDown">@php echo $value->details ?? ''; @endphp</p>
                                    <a href="{{ URL::to('contact') }}" class="redbutton wow flipInX">Transport with best</a>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="carousel_image2 wow zoomIn">
                                    <img src="{{ URL::to($value->image) }}" class="img-fluid">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                 @if(!empty($value->background_image) && !empty($value->image) && !empty($value->second_image) && !empty($value->third_image)  )

                 <div class="carousel-item {{ $key==0?'active':''}}">
                    <img src="{{ URL::to($value->background_image) }}" class="d-block w-100" alt="4">
                    <div class="redtruck_caption">
                        <div class="row">
                            <div class="col-6">
                                <div class="carousel_content caro-cont3">
                                    <h2 class="wow slideInLeft">{{ $value->title ?? '' }}</h2>
                                    <p class="wow slideInDown">@php echo $value->details ?? ''; @endphp</p>
                                    <a href="{{ URL::to('contact') }}" class="redbutton wow flipInX">Transport with best</a>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="carousel_image3">
                                    <div class=" banner3-rightimg1  wow fadeInDown">
                                        <img src="{{ URL::to($value->image) }}" class="img-fluid">
                                    </div>
                                    <div class="banner3-truck">
                                        <div class="banner3-rightimg2  wow slideInLeft">
                                            <img src="{{ URL::to($value->second_image) }}" class="img-fluid">
                                        </div>
                                        <div class="banner3-rightimg3  wow slideInRight">
                                            <img src="{{ URL::to($value->third_image) }}" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                  @endif
                

                @endforeach

               

            </div>
        </div>
    </section>
     @endif

    <!--About us-->
    <section class="home_aboutus">
        <div class="container">
            @php
            $about_us_meta = isset($about_us_meta_data->meta_data) && !empty($about_us_meta_data->meta_data) ? json_decode($about_us_meta_data->meta_data) : array() ;
            @endphp 

            <div class="home_aboutus_part1">
                <div class="row">
                    <div class="col-md-6">
                        <div class="aboutus_content">
                            <h1 class="left-title wow fadeInDown">About us</h1>
                            <p class="wow fadeInUp">{{ isset($about_us_meta->content) ? $about_us_meta->content : '' }}</p>
                            <a href="{{ URL::to('about') }}" class="redbutton redbtn_arrow wow flipInX">Read More</a>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="aboutus_video wow fadeInRight">
                            <video class="hmabvideo" width="100%" height="100%" controls poster="{{  isset($about_us_meta->video_poster_image) ? URL::to($about_us_meta->video_poster_image) : '' }}">
                                <source src="{{ isset($about_us_meta->video_url) ? URL::to($about_us_meta->video_url) : ''  }}" type="video/mp4">
                            </video>
                            <div class="playpause">
                                <img src="{{ URL::to(asset('images/frontend/svg/video-play-button.svg')) }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="home_aboutus_part2">
                <div class="row">
                     @php
                    $trust_meta = isset($trust_meta_data->meta_data) && !empty($trust_meta_data->meta_data) ? json_decode($trust_meta_data->meta_data) : array() ; 
                    @endphp
                    <div class="col-md-3">
                        <div class="aboutus_category">
                            <div class="aboutus_uppart">
                                <img src="{{ URL::to(asset('images/frontend/svg/deal.svg')) }}" class="img-fluid wow zoomIn">
                                <h4 class="wow fadeInDown">Trust</h4>
                            </div>
                            <p class="wow fadeInUp">{{ isset($trust_meta) && !empty($trust_meta->content) ? $trust_meta->content : '' }}</p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        @php
                        $security_meta = isset($security_meta_data->meta_data) && !empty($security_meta_data->meta_data) ? json_decode($security_meta_data->meta_data) : array() ; 
                        @endphp
                        <div class="aboutus_category">
                            <div class="aboutus_uppart">
                                <img src="{{ URL::to(asset('images/frontend/svg/padlock.svg')) }}" class="img-fluid wow zoomIn">
                                <h4 class="wow fadeInDown">Security</h4>
                            </div>
                            <p class="wow fadeInUp">{{ isset($security_meta) && !empty($security_meta->content) ? $security_meta->content : '' }}</p>

                        </div>
                    </div>
                    <div class="col-md-3">
                         @php
                        $sustainability_meta = isset($sustainability_meta_data->meta_data) && !empty($sustainability_meta_data->meta_data) ? json_decode($sustainability_meta_data->meta_data) : array() ; 
                        @endphp
                        <div class="aboutus_category">
                            <div class="aboutus_uppart">
                                <img src="{{ URL::to(asset('images/frontend/svg/shield.svg')) }}" class="img-fluid wow zoomIn">
                                <h4 class="wow fadeInDown">Sustainability</h4>
                            </div>
                            <p class="wow fadeInUp">{{ isset($sustainability_meta) && !empty($sustainability_meta->content) ? $sustainability_meta->content : '' }}</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        @php
                        $service_areas_meta = isset($service_areas_meta_data->meta_data) && !empty($service_areas_meta_data->meta_data) ? json_decode($service_areas_meta_data->meta_data) : array() ; 
                        @endphp
                        <div class="aboutus_category">
                            <div class="aboutus_uppart">
                                <img src="{{ URL::to(asset('images/frontend/svg/map.svg')) }}" class="img-fluid wow zoomIn">
                                <h4 class="wow fadeInDown">Service Areas</h4>
                            </div>
                            <p class="wow fadeInUp">{{ isset($service_areas_meta) && !empty($service_areas_meta->content) ? $service_areas_meta->content : '' }} </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <!--Services-->
    <section class="home_services">
        <h3 class="faded_heading">Services</h3>
        <div class="fade_background"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="services_list">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="{{ URL::to('service/logistics') }}">
                                    <div class="red_service">
                                        <img src="{{ URL::to(asset('images/frontend/svg/Asset-1.svg')) }}" class="img-fluid wow zoomIn">
                                        <h4 class="wow fadeInUp">Logistics</h4>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ URL::to('service/warehousing') }}">
                                    <div class="red_service">
                                        <img src="{{ URL::to(asset('images/frontend/svg/warehouse.svg')) }}" class="img-fluid wow zoomIn">
                                        <h4 class="wow fadeInUp">Warehousing</h4>
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-3">
                                <a href="{{ URL::to('service/transport') }}">
                                    <div class="red_service">
                                        <img src="{{ URL::to(asset('images/frontend/svg/steering-wheel.svg')) }}" class="img-fluid wow zoomIn">
                                        <h4 class="wow fadeInUp">Transport</h4>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ URL::to('service/transloading') }}">
                                    <div class="red_service">
                                        <img src="{{ URL::to(asset('images/frontend/svg/truck.svg')) }}" class="img-fluid wow zoomIn">
                                        <h4 class="wow fadeInUp">Transloading</h4>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    @php
                    $red_service_meta = isset($red_service_meta_data->meta_data) && !empty($red_service_meta_data->meta_data) ? json_decode($red_service_meta_data->meta_data) : array() ; 
                    @endphp
                    <div class="service_content">
                        <h2 class="wow fadeInDown">Red <br> Services</h2>
                        <p class="wow fadeInUp">{{ isset($red_service_meta) && !empty($red_service_meta->content) ? $red_service_meta->content : '' }} </p>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <!--Partner Section-->
    <!-- <section class="home_partner" style="background-image: url({{ URL::to(asset('images/frontend/home/logo-faded.png')) }})">
        <div class="container">
            <div class="row">
                @php
                $partner_meta = isset($partner_meta_data->meta_data) && !empty($partner_meta_data->meta_data) ? json_decode($partner_meta_data->meta_data) : array() ; 
                @endphp 
                <div class="col-md-12">
                    <div class="partner_content">
                        <h2 class="left-title wow fadeInDown">Partner</h2>
                        <p class="wow fadeInUp">{{ isset($partner_meta) && !empty($partner_meta->content) ? $red_service_meta->content : '' }}</p>
                        <a href="javascript:" class="redbutton redbtn_arrow wow flipInX">partner program</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
 -->


@if(isset($all_testimonials) && !empty($all_testimonials) )
    <!--Partner Section-->
    <section class="home_partner" style="background-image: url({{ URL::to(asset('images/frontend/home/logo-faded.png')) }})">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="partner_content">
                        <h2 class="text-white left-title wow fadeInDown">Testimonials</h2>
                        <div class="home-testimonial mt-3">
                            @foreach($all_testimonials as $key => $value)
                            <div>
                                <div class="slide-testimonial">
                                    <div class="testi-cont mb-4">
                                       <div class="align-center">
                                        <svg height="25px" class="mb-2 quote" fill="#f20105" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" xml:space="preserve">
                                                <g>
                                                    <g id="right_x5F_quote">
                                                        <g>
                                                            <path d="M0,4v12h8c0,4.41-3.586,8-8,8v4c6.617,0,12-5.383,12-12V4H0z" />
                                                            <path d="M20,4v12h8c0,4.41-3.586,8-8,8v4c6.617,0,12-5.383,12-12V4H20z" />
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                            <p class="mt-2">@php echo $value->details ?? ''; @endphp</p>
                                       </div>
                                    </div>

                                    <div class="testi-info d-flex align-items-center">
                                        <a href="javascript:"><img class="img-fluid" src="{{ URL::to($value->image) }}" alt="{{ $value->name ?? ''}}"></a>
                                        <div class="ml-4 testi-owinfo">
                                            <a href="javascript:" class="text-white">{{ $value->name ?? ''}}</a>
                                            <p class="text-white mb-0">{{  date('M d',strtotime($value->created_at)) ?? ''}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endif

    <!--Resources-->
     @if(isset($blog_data) && !empty($blog_data) )
    <section class="home_resources">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="resouces_box">
                        <div class="resource_blog_head">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="resbloghead_content">
                                        <h2 class="left-title wow fadeInLeft">Blogs </h2>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                     @php
                                    $resource_meta = isset($resource_meta_data->meta_data) && !empty($resource_meta_data->meta_data) ? json_decode($resource_meta_data->meta_data) : array() ; 
                                    @endphp 
                                    <div class="resbloghead_content">
                                       <!--  <p class="wow fadeInRight">{{ isset($resource_meta) && !empty($resource_meta->content) ? $resource_meta->content : '' }}</p> -->
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="resource_blog_box">
                            @foreach($blog_data as $key => $value)
                            <div class="box wow {{ $key==0 ? 'fadeInLeft' : ''}} {{ $key==1 ? 'fadeInUp' : ''}} {{ $key==2 ? 'fadeInRight' : ''}}">
                                <a href="{{ URL::to('blogs/detail/'.$value->blog_url) }}"><img src="{{ URL::to($value->image) }}" class="img-fluid"></a>
                                <div class="resblog_content">
                                    <a href="{{ URL::to('blogs/detail/'.$value->blog_url) }}">
                                        <h5>{{ $value->title ?? '' }} </h5>
                                    </a>
                                    <p> @php 
                                         $len = strlen($value->description) ;
                                           @endphp 
                                        
                                          {{ $value->sort_description ?? '' }}
                                    </p>
                                    <a class="blog_link" href="{{ URL::to('blogs/detail/'.$value->blog_url) }}"><img src="{{ URL::to(asset('images/frontend/svg/forward.svg')) }}"></a>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="resource_blog_foot">
                            <div class="row">
                                <div class="col-md-4 offset-md-2">
                        
                                </div>
                                <div class="col-md-6">
                                    <div class="resource_blog_viewall">
                                        <a href="{{ URL::to('blogs') }}" class="redbutton redbtn_arrow float-right wow flipInX">View All</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif

    @if(isset($all_clients) && !empty($all_clients) )
     <!--    Happy client-->
    <section class="home_happyclient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="">
                        <h2 class="left-title wow fadeInDown">Happy Clients</h2>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="">
                        <div class="home-happclient mt-3">
                            @foreach($all_clients as $key => $value)
                            <div>
                                <div class="slide-happyclient">
                                    <a href="{{ $value->url ??  'javascript:'}}" target="_blank" title="{{ $value->name ?? '' }}"><img src="{{ URL::to($value->image) }}" class="img-fluid" title="{{ $value->name ?? '' }}"></a>
                                </div>
                            </div>
                            @endforeach
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif

@endsection
@section('scripts')
@parent

@endsection