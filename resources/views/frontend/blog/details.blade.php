@extends('layouts.frontend')
@section('content')
<!--Banner-->
    <section class="webpage_banner" style="background-image: url({{ URL::to(asset('images/frontend/webbanner/contact-banner.jpg')) }})">
        <div class="row">
            <div class="col-md-12">
                <div class="webbanner-content">
                    <ul class="banner_breadcrumb">
                        <li><a href="{{ URL::to('/') }}">Home</a></li>
                        <li>Blogs</li>
                    </ul>
                    <h3 class="topbanner-head">{{ $blog_data->title ?? '' }}</h3>
                </div>
            </div>
        </div>
    </section>

     <section class="single_blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sgle_blogcont">
                        <div class="sgleblog_img wow fadeInUp"><img src="{{ URL::to($blog_data->image) }}" class="img-fluid"></div>
                        @php 
                         //$word_count = str_word_count($blog_data->description ?? '');
                        @endphp 
                        <p class="wow fadeInUp">@php echo $blog_data->description ?? '' @endphp</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
       @if(isset($recent_blog_data) && !empty($recent_blog_data) )

    <section class="recent_blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="recent_box">
                        <div class="recent_blog_head">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="recent_bloghead_content">
                                        <h2 class="left-title wow fadeInLeft">Recent Article</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="recent_blog_box mt-3">
                            <div class="row">
                                @foreach($recent_blog_data as $key => $value)
                                <div class="col-md-6">
                                    <div class="recent_blog-box wow fadeInUp mb-4 mb-md-0">
                                        <div class="row">
                                            <div class="col-md-12 col-xl-6">
                                                <div class="recent_blog_img wow fadeInLeft mb-3 mb-xl-0">
                                                    <a href="{{ URL::to('blogs/detail/'.$value->blog_url) }}"><img src="{{ URL::to($value->image) }}" class="img-fluid"></a>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-xl-6">
                                                <div class="recent_blog_content">
                                                    <a href="{{ URL::to('blogs/detail/'.$value->blog_url) }}">
                                                        <h5 class="wow fadeInUp">{{ $value->title ?? '' }}</h5>
                                                    </a>
                                                    <p class="wow fadeInDown">
                                                         {{ $value->sort_description ?? '' }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   @endif



@endsection
@section('scripts')
@parent

@endsection