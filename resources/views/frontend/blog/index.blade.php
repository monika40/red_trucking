@extends('layouts.frontend')
@section('content')
<!--Banner-->
    <section class="webpage_banner" style="background-image: url({{ URL::to(asset('images/frontend/webbanner/contact-banner.jpg')) }})">
        <div class="row">
            <div class="col-md-12">
                <div class="webbanner-content">
                    <ul class="banner_breadcrumb">
                        <li><a href="{{ URL::to('/') }}">Home</a></li>
                        <li>Blogs</li>
                    </ul>
                    <h3 class="topbanner-head">BLOGS</h3>
                </div>
            </div>
        </div>
    </section>

    @if(isset($blog_data) && !empty($blog_data) )
    <section class="blog_featured">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="featured_box">
                        <div class="featured_blog_head">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="bloghead_content">
                                        <h1 class="left-title wow fadeInLeft">Featured Article</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                       
                        <div class="featured_blog_box mt-4">
                            <div class="row">
                                 @foreach($blog_data as $key => $value)
                                <div class="col-md-4">
                                    <div class="blog-box wow fadeInLeft mb-5 mb-md-0">
                                        <a href="{{ URL::to('blogs/detail/'.$value->blog_url) }}"><img src="{{ URL::to($value->image) }}" alt="{{ $value->title ?? '' }}" class="img-fluid"></a>
                                        <div class="blog_content">
                                            <a href="{{ URL::to('blogs/detail/'.$value->blog_url) }}">
                                                <h5 class="wow fadeInUp">{{ $value->title ?? '' }}</h5>
                                            </a>
                                            <p class="wow fadeInDown">
                                               
                                                {{ $value->sort_description ?? '' }}
                                            </p>
                                            <a href="{{ URL::to('blogs/detail/'.$value->blog_url) }}" class="redbutton redbtn_arrow wow flipInX">Read More</a>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                              
                        </div>
                        
                       
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="blog_pagination mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pagination_box">
                        {!! $blog_data->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>


    @else 
     <section class="blog_pagination mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pagination_box">
                       <br/>
                       <br/>
                       <br/>
                       <br/>
                       <br/>
                       <br/>
                       <br/>
                       <br/>
                       <br/>

                       <h1> No Blog Avaiable !</h1>
                       <br/>
                       <br/>
                       <br/>
                       <br/>
                       <br/>
                       <br/>
                       <br/>
                       <br/>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif



@endsection
@section('scripts')
@parent

@endsection