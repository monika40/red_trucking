@extends('layouts.frontend')
@section('content')

<!--Banner-->
    <section class="webpage_banner" style="background-image: url({{ URL::to(asset('images/frontend/webbanner/safety-banner.jpg')) }})">
        <div class="row">
            <div class="col-md-12">
                <div class="webbanner-content">
                    <ul class="banner_breadcrumb">
                        <li><a href="{{ URL::to('/') }}">Home</a></li>
                        <li>About Us</li>
                    </ul>
                    <h1 class="topbanner-head">Safety</h1>
                </div>
            </div>
        </div>
    </section>


     @php
    $safety_meta = isset($safety_data->meta_data) && !empty($safety_data->meta_data) ? json_decode($safety_data->meta_data) : array() ;
    @endphp 

    <!--Safety Body-->
    <section class="safety-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="safetop-content">
                        <p class="wow fadeInDown">{{ isset($safety_meta->title) ? $safety_meta->title : '' }} </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="safety-left pt-4 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="safeleft-img wow fadeInLeft">
                        <img src="{{  isset($safety_meta->left_image) && file_exists($safety_meta->left_image) ? URL::to($safety_meta->left_image):''  }}" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-7 d-flex align-items-center">
                    <div class="safeleft-content">
                        <p class=" wow fadeInUp mb-0">{{ isset($safety_meta->left_content) ? $safety_meta->left_content : '' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="safety-middle">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="safemid-content">
                        <p class="text-white wow fadeInUp">{{ isset($safety_meta->middle_content) ? $safety_meta->middle_content : '' }} </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="safety-right py-5">
        <h3 class="saferght-faded wow slideInUp">Safety</h3>
        <div class="container">
            <div class="row">
                <div class="col-md-7 d-flex align-items-center">
                    <div class="saferght-content mr-5  wow fadeInUp">
                        <p class="mb-0">{{ isset($safety_meta->right_content) ? $safety_meta->right_content : '' }} </p>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="saferght-img  wow slideInRight">
                        <img src="{{  isset($safety_meta->right_image) && file_exists($safety_meta->right_image) ? URL::to($safety_meta->right_image):''  }}" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
@section('scripts')
@parent

@endsection