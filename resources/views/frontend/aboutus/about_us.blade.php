@extends('layouts.frontend')
@section('content')

<!--Banner-->
    <section class="webpage_banner" style="background-image: url({{ URL::to(asset('images/frontend/webbanner/about-banner.jpg')) }})">
        <div class="row">
            <div class="col-md-12">
                <div class="webbanner-content">
                    <ul class="banner_breadcrumb">
                        <li><a href="{{ URL::to('/') }}">Home</a></li>
                        <li>About</li>
                    </ul>
                    <h3 class="topbanner-head">About Us</h3>
                </div>
            </div>
        </div>
    </section>

    <!--About Us Body-->
    <section class="aboutus-ceomessage">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                     @php
                    $about_us_owner_meta = isset($about_us_owner_data->meta_data) && !empty($about_us_owner_data->meta_data) ? json_decode($about_us_owner_data->meta_data) : array() ;
                    @endphp 
                    <div class="top-ceomessage text-center">
                        <h5 class="wow fadeInDown">{{ isset($about_us_owner_meta->owner_message)&& !empty($about_us_owner_meta->owner_message) ? $about_us_owner_meta->owner_message :''}}</h5>
                        <img src=" {{ isset($about_us_owner_meta->signature)&& !empty($about_us_owner_meta->signature) ? URL::to($about_us_owner_meta->signature) :''}}" class="img-fluid wow zoomIn">
                        <p class="wow fadeInUp"><span>- {{ isset($about_us_owner_meta->designation)&& !empty($about_us_owner_meta->designation) ? $about_us_owner_meta->designation :''}}</span> </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="aboutus-whatwedo">
        <div class="faded-background-whatwedo"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="whatwedo-content">
                        <h1 class="left-title wow fadeInDown">{{ isset($abouts_us_data->title) ? $abouts_us_data->title : '' }}</h1>
                        <p class="wow fadeInUp">{{ isset($abouts_us_data->description) ? $abouts_us_data->description : '' }} </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="whatwedo-image">
                        <img src="{{  isset($left_about_us_image->name) && file_exists($left_about_us_image->name) ? URL::to($left_about_us_image->name):''  }}" class="img-fluid wow fadeInLeft">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="whatwedo-image">
                        <img src="{{  isset($right_about_us_image->name) && file_exists($right_about_us_image->name) ? URL::to($right_about_us_image->name):''  }}" class="img-fluid wow fadeInRight">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="aboutus-ourmission">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ourmission-content">
                        <h2 class="left-title wow fadeInDown">Our Misssion</h2>
                        <div class="row"> <?php echo isset($abouts_us_data->sort_description) ? $abouts_us_data->sort_description : '' ;  ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @php
    $safety_meta = isset($safety_data->meta_data) && !empty($safety_data->meta_data) ? json_decode($safety_data->meta_data) : array() ;
    @endphp 

    @if(isset($safety_meta) && !empty($safety_meta))
    <section class="aboutus-safety">
        <div class="faded-background-safety"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 pr-0">
                    <div class="safety-image">
                        <img src="{{ isset($safety_meta->safety_images) ? URL::to($safety_meta->safety_images) : ''}}" class="img-fluid wow slideInLeft">
                    </div>
                </div>
                <div class="col-md-6 pl-0 d-flex align-items-center">
                    <div class="safety-content">
                        <h4 class="sub_heading wow fadeInDown">SAFETY</h4>
                        <p class="wow fadeInUp">{{ isset($safety_meta->content) ? $safety_meta->content : ''}}</p>
                        <a href="{{ URL::to('about/safety') }}" class="redbutton redbtn_arrow wow flipInX">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif

    @php
    $sustainability_meta = isset($sustainability_data->meta_data) && !empty($sustainability_data->meta_data) ? json_decode($sustainability_data->meta_data) : array() ;
    @endphp 
    
    @if(isset($sustainability_meta) && !empty($sustainability_meta))
    <section class="aboutus-sustain">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 pr-0 d-flex align-items-center">
                    <div class="sustain-content">
                        <h4 class="sub_heading wow fadeInDown">SUSTAINABILITY</h4>
                        <p class="wow fadeInUp">{{ isset($sustainability_meta->content) ? $sustainability_meta->content : ''}}</p>
                        <a href="{{ URL::to('about/sustainability') }}" class="redbutton redbtn_arrow wow flipInX">Read More</a>
                    </div>
                </div>
                <div class="col-md-6 pl-0">
                    <div class="sustain-image">
                        <img src="{{ isset($sustainability_meta->sustainability_images) ? URL::to($sustainability_meta->sustainability_images) : ''}}" class="img-fluid wow slideInRight">
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    <section class="aboutus-redfamily">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @php
                    $red_family__meta = isset($red_family_data->meta_data) && !empty($red_family_data->meta_data) ? json_decode($red_family_data->meta_data) : array() ;
                    @endphp 
                    <div class="redfamily-content">
                        <h2 class="left-title wow fadeInDown">The Red Family</h2>
                        <p class="wow fadeInUp">{{ isset($red_family__meta->content) ? $red_family__meta->content : '' }}</p>
                    </div>
                    @if(!empty($all_members) && sizeof($all_members))
                    <div class="redfamily-teamimage">
                        <div class="row">
                             @foreach($all_members as $key => $value)
                            <div class="col-md-4">
                                <div class="redfamily-box wow slideInLeft">
                                    <div class="redfamily-image">
                                        <img src="{{ URL::to($value->image) }}" class="img-fluid" alt="{{ $value->name}}">
                                    </div>
                                    <div class="redfamily-teamname">
                                        <h6>{{ $value->name}}</h6>
                                        <small>{{ $value->designation}}</small>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                     @endif
                </div>
            </div>
        </div>
    </section>


@endsection
@section('scripts')
@parent

@endsection