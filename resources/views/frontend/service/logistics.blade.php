@extends('layouts.frontend')
@section('content')

<!--Banner-->
    <section class="webpage_banner" style="background-image: url({{ URL::to(asset('images/frontend/webbanner/service-banner.jpg')) }})">
        <div class="row">
            <div class="col-md-12">
                <div class="webbanner-content">
                    <ul class="banner_breadcrumb">
                        <li><a href="{{ URL::to('/') }}">Home</a></li>
                        <li>Services</li>
                    </ul>
                    <h3 class="topbanner-head">LOGISTICS</h3>
                </div>
            </div>
        </div>
    </section>
    @php
    $data = isset($logistics_data->meta_data) && !empty($logistics_data->meta_data) ? json_decode($logistics_data->meta_data) : array() ;
    
    @endphp 

    @include('frontend.service.service_tab')

   <section class="service_listbox">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="service_listbox_content mb-3 mb-lg-5">
                        <h1 class="sub_heading wow fadeInDown">{{ $data->section_one_title ?? '' }}</h1>
                        <p class="wow fadeInUp">{{ $data->section_one_first_paragraphp ?? '' }}
                        <p class="wow fadeInUp"><strong>{{ $data->section_one_second_paragraphp ?? '' }}</strong></p>
                    </div>
                    <div class="service_listbox_content mb-3 mb-lg-5">
                        <h1 class="sub_heading wow fadeInDown">{{ $data->section_one_second_title ?? '' }}</h1>
                        <p class="wow fadeInUp">{{ $data->section_one_second_description ?? '' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="service_listbox_middle">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="service_listbox_midcontent">
                        <div class="service_listbox_content mb-3 mb-lg-5">
                            <h4 class="sub_heading wow fadeInDown">{{ $data->section_two_first_title ?? '' }}</h4>
                            <p class="wow fadeInUp">{{ $data->section_two_first_description ?? '' }}</p>
                        </div>
                        <div class="service_listbox_content">
                            <h4 class="sub_heading wow fadeInDown">{{ $data->section_two_second_title ?? '' }}</h4>
                            <p class="wow fadeInUp">{{ $data->section_two_second_description ?? '' }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="service_listbox mb-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-6  order-2 order-lg-1">
                    <div class="service_listbox_content mr-lg-5 mr-0">
                        <div class="service_listbox_content mb-3 mb-lg-5">
                            <h4 class="sub_heading wow fadeInDown">{{ $data->section_third_first_title ?? '' }}</h4>
                            <p class="wow fadeInUp">{{ $data->section_three_first_description ?? '' }} </p>
                        </div>

                        <div class="service_listbox_content mb-3 mb-lg-5">
                            <h4 class="sub_heading wow fadeInDown">{{ $data->section_three_second_title ?? '' }}</h4>
                            <p class="wow fadeInUp">{{ $data->section_three_second_description ?? '' }} </p>
                        </div>
                        <div class="service_listbox_content mb-3 mb-lg-5">
                            <h4 class="sub_heading wow fadeInDown">{{ $data->section_third_third_title ?? '' }}</h4>
                            <p class="wow fadeInUp">{{ $data->section_third_third_description ?? '' }} </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 d-flex align-items-center order-1 order-lg-2">
                    <div class="logistic_img wow fadeInRight  mb-5">
                        <img src="{{ URL::to($data->section_three_image) }}" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </section>


     @include('frontend.service.contact_us')

@endsection
@section('scripts')

@parent

@endsection