<section class="service-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="service-top">
                        <!-- Nav tabs -->
                        <ul class="service-folder border-bottom-0">
                            <li class="service-list wow flipInX">
                                <a class="service-link {{ request()->is('service/transport') ? 'active' : '' }}" href="{{ URL::to('service/transport')}}">Transport</a>
                            </li>

                            <li class="service-list wow fadeIn">
                                <a class="service-link {{ request()->is('service/warehousing') ? 'active' : '' }}" href="{{ URL::to('service/warehousing')}}">Warehousing</a>
                            </li>
                            <li class="service-list wow fadeIn">
                                <a class="service-link {{ request()->is('service/logistics') ? 'active' : '' }} " href="{{ URL::to('service/logistics')}}">Logistics</a>
                            </li>

                            <li class="service-list wow fadeIn">
                                <a class="service-link {{ request()->is('service/transloading') ? 'active' : '' }}" href="{{ URL::to('service/transloading')}}">Transloading</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</section>