
   <section class="contact-sendmessage">
        <div class="faded-background-sendmessage"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="row">
                        <div class="col-md-5">
                            <div class="sendmessage-imgleft wow slideInLeft">
                               <img src="{{ URL::to(asset('images/frontend/contact/contact.png')) }}" class="img-fluid">
                            </div>

                        </div>
                        <div class="col-md-7">
                            <div class="sendmessage-content">
                                <h2 class="left-title wow fadeInDown">Send Message</h2>
                            </div>
                            
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success" role="alert">
                                {{ $message }}
                            </div>
                            @endif
                            @if ($message = Session::get('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ $message }}
                            </div>
                            @endif

                            <div class="contact-form mt-4">
                                    <form action="{{ URL::to('contact/save') }}"  name="contactregister" method="POST" enctype="multipart/form-data">
                                         @csrf
                                    <div class="form-row">
                                        <div class="form-group col-md-6 wow fadeInUp">
                                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name*" value="{{ old('first_name', '') }}" maxlength="20" >
                                             @if($errors->has('first_name'))
                                                <p class="error">
                                                    {{ $errors->first('first_name') }}
                                                </p>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 wow fadeInUp">
                                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{ old('last_name', '') }}" maxlength="20" >
                                            @if($errors->has('last_name'))
                                                <p class="error">
                                                    {{ $errors->first('last_name') }}
                                                </p>
                                            @endif
                                            
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6 wow fadeInUp">
                                          
                                             <input type="email" class="form-control" id="email" name="email" placeholder="Email Address*" value="{{ old('email', '') }}">
                                            @if($errors->has('email'))
                                                    <p class="error">
                                                        {{ $errors->first('email') }}
                                                    </p>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 wow fadeInUp">
                                            <input type="text" class="form-control is_number" maxlength="10" id="phone" name="phone" placeholder="Phone Number*" value="{{ old('phone', '') }}"  >
                                            @if($errors->has('phone'))
                                                    <p class="error">
                                                        {{ $errors->first('phone') }}
                                                    </p>
                                            @endif
                                            
                                        </div>
                                    </div>

                                    <div class="form-group wow fadeInUp">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" id="optradio1" name="transport_type" value="1" checked>
                                            <span class="checkmark"></span>
                                            <label class="form-check-label" for="optradio1">Warehouse / Transloading</label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" id="optradio2" name="transport_type" value="2">
                                            <span class="checkmark"></span>
                                            <label class="form-check-label" for="optradio2">Sales Quotes</label>
                                        </div>
                                        @if($errors->has('transport_type'))
                                                    <p class="error">
                                                        {{ $errors->first('transport_type') }}
                                                    </p>
                                        @endif
                                    </div>
                                    <div class="cardinfobox">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" id="compname" name="company_name" placeholder="Company Name*" value="{{ old('company_name', '') }}">
                                                 @if($errors->has('company_name'))
                                                    <p class="error">
                                                        {{ $errors->first('company_name') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" id="commodity" name="commodity" placeholder="Commodity*" value="{{ old('commodity', '') }}">
                                                 @if($errors->has('commodity'))
                                                    <p class="error">
                                                        {{ $errors->first('commodity') }}
                                                    </p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" id="noload" name="number_of_loads" placeholder="Number Of Loads" value="{{ old('number_of_loads', '') }}">
                                                 @if($errors->has('number_of_loads'))
                                                    <p class="error">
                                                        {{ $errors->first('number_of_loads') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" name="cargo_weight" id="carweight" placeholder="Cargo Weight" value="{{ old('cargo_weight', '') }}">
                                                 @if($errors->has('cargo_weight'))
                                                    <p class="error">
                                                        {{ $errors->first('cargo_weight') }}
                                                    </p>
                                                @endif
                                            </div>
                                        </div>

                                        <label>Origin Information</label>
                                        <div class="form-row">
                                            <div class="form-group col-md-8">
                                                <input type="text" class="form-control" id="originadd" placeholder="Pick Up Address" name="pickup_address" value="{{ old('pickup_address', '') }}">
                                                 @if($errors->has('pickup_address'))
                                                    <p class="error">
                                                        {{ $errors->first('pickup_address') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                <input type="text" class="form-control" id="origincity" placeholder="City" name="pickup_city" value="{{ old('pickup_city', '') }}">
                                                 @if($errors->has('pickup_city'))
                                                    <p class="error">
                                                        {{ $errors->first('pickup_city') }}
                                                    </p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <input type="text" class="form-control" id="originstate" placeholder="State" name="pickup_state" value="{{ old('pickup_state', '') }}">
                                                 @if($errors->has('pickup_state'))
                                                    <p class="error">
                                                        {{ $errors->first('pickup_state') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-3">
                                                <input type="text" class="form-control" id="originzip" placeholder="Zip Code" name="pickup_zip_code" value="{{ old('pickup_zip_code', '') }}">
                                                 @if($errors->has('pickup_zip_code'))
                                                    <p class="error">
                                                        {{ $errors->first('pickup_zip_code') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-5">
                                                <input type="text" class="form-control" id="originineq" placeholder="Intermodal Equipment" name="pickup_intermodal_equipment" value="{{ old('pickup_intermodal_equipment', '') }}">
                                                 @if($errors->has('pickup_intermodal_equipment'))
                                                    <p class="error">
                                                        {{ $errors->first('pickup_intermodal_equipment') }}
                                                    </p>
                                                @endif
                                            </div>
                                        </div>

                                        <label>Destination Information</label>
                                        <div class="form-row">
                                            <div class="form-group col-md-8">
                                                <input type="text" class="form-control" id="originadd" placeholder="Termination Address" name="destination_address" value="{{ old('last_name', '') }}">
                                                 @if($errors->has('destination_address'))
                                                    <p class="error">
                                                        {{ $errors->first('destination_address') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-4">
                                                <input type="text" class="form-control" id="origincity" placeholder="City" name="destination_city" value="{{ old('destination_city', '') }}">
                                                 @if($errors->has('destination_city'))
                                                    <p class="error">
                                                        {{ $errors->first('destination_city') }}
                                                    </p>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <input type="text" class="form-control" id="originstate" placeholder="State" name="destination_state" value="{{ old('destination_state', '') }}">
                                                 @if($errors->has('destination_state'))
                                                    <p class="error">
                                                        {{ $errors->first('destination_state') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-3">
                                                <input type="text" class="form-control" id="originzip" placeholder="Zip Code" name="destination_zip_code" value="{{ old('destination_zip_code', '') }}">
                                                 @if($errors->has('destination_zip_code'))
                                                    <p class="error">
                                                        {{ $errors->first('destination_zip_code') }}
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-5">
                                                <input type="text" class="form-control" id="originineq" placeholder="Intermodal Equipment" name="destination_intermodal_equipment" value="{{ old('destination_intermodal_equipment', '') }}">
                                                 @if($errors->has('destination_intermodal_equipment'))
                                                    <p class="error">
                                                        {{ $errors->first('destination_intermodal_equipment') }}
                                                    </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row d-flex align-items-center wow fadeInUp">
                                        <div class="form-group col-md-12 col-lg-6">
                                            <label class="mb-0">Best time to connect with you </label>
                                        </div>
                                        <!--
                                        <div class="form-group offset-md-2 col-md-4 d-flex align-items-center">
                                            <input type="time" class="form-control" >
                                        </div>
-->
                                        <div class="form-group col-md-12  col-lg-6 d-flex align-items-center">
                                            <select class="form-control" id="best_time_hour" name="best_time_hour">
                                                <option value="" disabled selected hidden>HH</option>
                                                <option value="01">01</option>
                                                <option value="02">02</option>
                                                <option value="03">03</option>
                                                <option value="04">04</option>
                                                <option value="05">05</option>
                                                <option value="06">06</option>
                                                <option value="07">07</option>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                            <span>:</span>
                                             @if($errors->has('best_time_hour'))
                                                <p class="error">
                                                    {{ $errors->first('best_time_hour') }}
                                                </p>
                                            @endif
                                            <select class="form-control" id="best_time_minute" name="best_time_minute">
                                                <option value="" disabled selected hidden>MM</option>
                                                <option value="05">00</option>
                                                <option value="05">05</option>
                                                <option value="10">10</option>
                                                <option value="15">15</option>
                                                <option value="20">20</option>
                                                <option value="25">25</option>
                                                <option value="30">30</option>
                                                <option value="35">35</option>
                                                <option value="40">40</option>
                                                <option value="45">45</option>
                                                <option value="50">50</option>
                                                <option value="55">55</option>
                                                <option value="60">60</option>
                                            </select>
                                            @if($errors->has('best_time_minute'))
                                                <p class="error">
                                                    {{ $errors->first('best_time_minute') }}
                                                </p>
                                            @endif
                                            <select class="form-control" id="best_time" name="best_time">
                                                <option value="" disabled selected hidden>AM</option>
                                                <option value="am" selected>AM</option>
                                                <option value="pm">PM</option>
                                            </select>
                                            @if($errors->has('best_time'))
                                                <p class="error">
                                                    {{ $errors->first('best_time') }}
                                                </p>
                                           @endif
                                        </div>
                                    </div>
                                    <div class="form-group wow fadeInUp">
                                        <textarea class="form-control" id="message" name="message" rows="7" placeholder="Message"></textarea>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12 col-lg-8 ">
                                            <label class="submit__control">
                                                <div class="submit__generated1 unvalid">
                                                    <input type="number" class="first_number is_number" id="first_number" name="first_number" readonly="readonly"> + <input type="number" class="second_number is_number" id="second_number" name="second_number" readonly="readonly"> = 
                                                    <input class="submit__input is_number" maxlength="2" type="text" maxlength="2" size="2" id="result" name="capture" value="{{ old('capture', '') }}" onkeyup ="validateCapture()">
                                                    @if($errors->has('capture'))
                                                        <p class="error">
                                                            {{ $errors->first('capture') }}
                                                        </p>
                                                    @endif
                                                </div>
                                                <p class="result_error"></p>
                                                <i class="fas fa-sync refersh_capture"></i>
                                            </label>
                                        </div>

                                        <div class="form-group col-md-12 col-lg-4">
                                            <input class="mt-0 border-0 float-lg-right submit overlay redbutton redbtn_arrow wow flipInX" type="submit" value="Submit >" />
                                            <div class="submit__overlay"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>