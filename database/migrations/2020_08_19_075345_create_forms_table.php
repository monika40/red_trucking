<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {

            $table->id();
            $table->string('transport_type',10)->nullable()->comment('1->Warehouse / Transloading, 2->Sales Quotes')->nullable();
            $table->tinyInteger('form_type')->nullable()->comment('1->Contact, 2->Career')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('best_time')->nullable();
            $table->string('subject')->nullable();
            $table->longText('message')->nullable();
            $table->string('upload_file')->nullable();
            $table->tinyInteger('status')->length(1)->default(1);
            $table->tinyInteger('is_deleted')->length(1)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
