<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_log', function (Blueprint $table) {
            $table->id();
            $table->integer('parent_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('item_id')->nullable();
            $table->integer('item_type')->nullable()->comment('1->Login, 2->Logout');
            $table->string('log_title')->nullable();
            $table->string('log_text')->nullable();
            $table->longText('extra_data')->nullable();
            $table->string('ip_address')->nullable();
            $table->tinyInteger('status')->length(1)->default(1);
            $table->tinyInteger('is_deleted')->length(1)->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_log');
    }
}
